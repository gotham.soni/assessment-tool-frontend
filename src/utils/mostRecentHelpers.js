export function getRecentItems(data) {
  if (data !== undefined) {
    // get dates and index of all projects
    var dates = [];
    for (var form = 0; form < data.length; form++) {
      data[form].history // get date from history array, or else from mongo _id
        ? dates.push({
            date: new Date(data[form].history[0]._date),
            index: form
          })
        : dates.push({
            date: new Date(idToDate(data[form]._id, true)),
            index: form
          });
    }
    // sort by date, newest first
    dates.sort((a, b) => {
      return b.date - a.date;
    });

    // return four most recent projects
    var retVal = [];
    var maxAmount;

    dates.length < 4 ? (maxAmount = dates.length) : (maxAmount = 4);

    for (var x = 0; x < maxAmount; x++) {
      retVal.push(data[dates[x].index]);
    }

    return retVal;
  }
}

export function idToDate(id) {
  return new Date(parseInt(id.toString().substring(0, 8), 16) * 1000);
}

export function getFormattedDate(date, isID, includeTime) {
  if (isID) {
    date = idToDate(date);
    var dd = String(date.getDate()).padStart(2, "0");
    var mm = String(date.getMonth() + 1).padStart(2, "0"); //January is 0!
    var yyyy = date.getFullYear();
    var minutes =
      date.getMinutes() < 10
        ? "0" + String(date.getMinutes())
        : String(date.getMinutes());
    var time = String(date.getHours()) + ":" + minutes;
    if (includeTime) {
      date = mm + "/" + dd + "/" + yyyy + " at " + time;
      return date;
    }
    return mm + "/" + dd + "/" + yyyy;
  } else {
    date = date.split("/");
    var dayAndTime = date[2].split(" ");
    time = dayAndTime[1].split(":");
    if (time[1].length === 1) {
      time[1] = "0" + time[1];
    }
    time = time[0] + ":" + time[1];
    if (includeTime) {
      date = date[1] + "/" + dayAndTime[0] + "/" + date[0] + " at " + time;
      return date;
    }
    return date[1] + "/" + dayAndTime[0] + "/" + date[0];
  }
}

export function getRecentAuthor(form) {
  if (form.history) {
    return form.history[form.history.length - 1].author;
  }
  return form.author;
}
