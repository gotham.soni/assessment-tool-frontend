import { combineReducers } from "redux";

import DashboardDataTableReducer from './DashboardDataTable/dashboardDataTable-reducers';
import ModuleReducer from './Modules/modules-reducers';
import authReducer from "./Login/login-reducers";
import errorReducer from "./Login/error-reducers";
import SearchReducer from "./Search/search-reducers"
import { reducer as formReducer } from 'redux-form';

export default combineReducers({
    search: SearchReducer,
    dashboardDataTable: DashboardDataTableReducer,
    modules: ModuleReducer,
    form: formReducer,
    auth: authReducer,
    errors: errorReducer
});