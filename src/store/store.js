//import { applyMiddleware, createStore } from "redux";
import thunk from 'redux-thunk'
import reducers from './index'
import {applyMiddleware, compose, createStore} from "redux";


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const initialState = {}
const store = createStore(
    reducers, initialState,
    composeEnhancers(applyMiddleware(thunk))
);


export default store;