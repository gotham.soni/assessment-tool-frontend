import {
    FETCH_ASSESSMENT_FORM_REQUEST,
    FETCH_ASSESSMENT_FORM_SUCCESS,
    FETCH_ASSESSMENT_FORM_FAILURE,
    UPDATE_ASSESSMENT_FORM,
    POST_ASSESSMENT_FORM,
    LOAD_FORM_TO_UPDATE,
    IS_NEW_ASSESSMENT,
    MODULES_FOR_ASSESSMENT
} from './dashboardDataTable-types';

const INITIAL_STATE = {
    fetchingAssessmentForms: null,
    items: { success: true, data: [] },
    newAssessment: {},
    form: {},
    updateForm: {},
    isNewAssessment: true,
    modules_for_assessment:[]
};

const DashboardDataTableReducer = (state = INITIAL_STATE, { type, payload }) => {
    switch (type) {
        case FETCH_ASSESSMENT_FORM_REQUEST:
            return {
                ...state,
                items: payload
            };
        case FETCH_ASSESSMENT_FORM_FAILURE:
            return {
                ...state,
                items: payload
            }
        case FETCH_ASSESSMENT_FORM_SUCCESS:
            return {
                ...state,
                items: payload
            };
        case POST_ASSESSMENT_FORM:
            return {
                ...state,
                newAssessment: payload
            };
        case UPDATE_ASSESSMENT_FORM:
            return {
                ...state,
                updateForm: payload
            };
        case LOAD_FORM_TO_UPDATE:
            return {
                ...state,
                form: payload
            }
        case IS_NEW_ASSESSMENT:
            return {
                ...state,
                isNewAssessment: payload
            }
        case MODULES_FOR_ASSESSMENT:
            return {
                ...state,
                modules_for_assessment: payload
            }
        default:
            return state;
    }
}

export default DashboardDataTableReducer;