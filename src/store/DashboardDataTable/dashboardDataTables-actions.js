import {
  FETCH_ASSESSMENT_FORM_REQUEST,
  POST_ASSESSMENT_FORM,
  LOAD_FORM_TO_UPDATE,
  IS_NEW_ASSESSMENT,
  MODULES_FOR_ASSESSMENT
} from "./dashboardDataTable-types";

import { fetchAssessmentForms } from "../../services/assessmentForm-service";
import {
  postAssessmentForm,
  updateAssessmentForm
} from "../../services/assessmentForm-service";
import { fetchModules } from "../../services/module-service";

export const asyncFetchAssessmentForms = sort => async dispatch => {
  const forms = await fetchAssessmentForms(sort);
  dispatch({ type: FETCH_ASSESSMENT_FORM_REQUEST, payload: forms });
};

export const asyncPostForm = form => async dispatch => {
  const formPut = await postAssessmentForm(form);
  dispatch({ type: POST_ASSESSMENT_FORM, payload: formPut });
};

export const asyncLoadFormToUpdate = form => dispatch => {
  dispatch({ type: LOAD_FORM_TO_UPDATE, payload: form });
};

export const asyncUpdateForm = (form, formId) => async dispatch => {
  const formUpdate = await updateAssessmentForm(form, formId);
  dispatch({ type: POST_ASSESSMENT_FORM, payload: formUpdate });
};

export const isNewAssessment = isNew => dispatch => {
  dispatch({ type: IS_NEW_ASSESSMENT, payload: isNew });
};

export const asyncGetModulesForAssessments = () => async dispatch => {
  const modules = await fetchModules();
  var m2a = [];
  var mdata = modules.data;
  if (mdata === undefined) {
    mdata = [];
  }

  for (var element of mdata) {
    var optionObject = {};
    optionObject.label = element.name;
    optionObject.value = element.name;
    if (element.subType) {
      optionObject.subType = element.subType;
    }
    optionObject.type = element.type;
    m2a.push(optionObject);
  }
  dispatch({ type: MODULES_FOR_ASSESSMENT, payload: m2a });
};
