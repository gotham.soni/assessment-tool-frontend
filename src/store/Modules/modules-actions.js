import {
  FETCH_MODULE_FORM_REQUEST,
  POST_MODULE_FORM,
  LOAD_MODULE_TO_UPDATE,
  IS_NEW_MODULE,
  ASSESSMENTS_FOR_MODULES
} from "./module-types";

import {
  fetchModules,
  postNewModule,
  updateModuleForm
} from "../../services/module-service";

import { fetchAssessmentForms } from "../../services/assessmentForm-service";

export const asyncFetchModules = sort => async dispatch => {
  if (!sort) {
    sort = { sortBy: false, order: "asc" };
  }
  const modules = await fetchModules(sort);
  dispatch({ type: FETCH_MODULE_FORM_REQUEST, payload: modules });
};

export const asyncPostModuleForm = newModule => async dispatch => {
  const modulePut = await postNewModule(newModule);
  dispatch({ type: POST_MODULE_FORM, payload: modulePut });
};

export const asyncLoadModuleToUpdate = moduleToUpdate => dispatch => {
  dispatch({ type: LOAD_MODULE_TO_UPDATE, payload: moduleToUpdate });
};

export const asyncUpdateModuleForm = (uModule, moduleId) => async dispatch => {
  const moduleUpdate = await updateModuleForm(uModule, moduleId);
  dispatch({ type: POST_MODULE_FORM, payload: moduleUpdate });
};

export const isNewModule = isNewModule => dispatch => {
  dispatch({ type: IS_NEW_MODULE, payload: isNewModule });
};

export const asyncGetAssessmentsForModules = () => async dispatch => {
  var assessments = await fetchAssessmentForms();
  var a2m = [];
  try {
    if (assessments !== undefined) {
      assessments.data.forEach(element => {
        var optionObject = {};
        optionObject.label = element.name;
        optionObject.value = element.name;
        optionObject.modules = element.estimates
          .map(estimate => estimate.modules.map(module => module.name))
          .flat();
        a2m.push(optionObject);
      });
    }
  } catch (e) {}
  dispatch({ type: ASSESSMENTS_FOR_MODULES, payload: a2m });
};
