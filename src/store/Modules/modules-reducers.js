import {
    FETCH_MODULE_FORM_REQUEST,
    FETCH_MODULE_FORM_SUCCESS,
    FETCH_MODULE_FORM_FAILURE,
    UPDATE_MODULE_FORM,
    POST_MODULE_FORM,
    LOAD_MODULE_TO_UPDATE,
    IS_NEW_MODULE,
    ASSESSMENTS_FOR_MODULES
} from './module-types';

const INITIAL_STATE = {
    fetchingModules: null,
    mods: { success: true, data: [] },
    newModule: {},
    module: {},
    updateModule: {},
    isNewModule: true,
    assessments_for_modules : []
};

const ModuleReducer = (state = INITIAL_STATE, { type, payload }) => {
    switch (type) {
        case FETCH_MODULE_FORM_REQUEST:
            return {
                ...state,
                mods: payload
            };
        case FETCH_MODULE_FORM_SUCCESS:
            return {
                ...state,
                mods: payload
            }
        case FETCH_MODULE_FORM_FAILURE:
            return {
                ...state,
                mods: payload
            };
        case UPDATE_MODULE_FORM:
            return {
                ...state,
                updateModule: payload
            };
        case POST_MODULE_FORM:
            return {
                ...state,
                newModule: payload
            };
        case LOAD_MODULE_TO_UPDATE:
            return {
                ...state,
                module: payload
            }
        case IS_NEW_MODULE:
            return {
                ...state,
                isNewModule: payload
            }
        case ASSESSMENTS_FOR_MODULES:
            return {
                ...state,
                assessments_for_modules: payload
            }
        default:
            return state;
    }
}

export default ModuleReducer;