import{
    SEARCH_QUERY,
    SEARCH_SUCCESS, 
    UPDATE_INPUT
} from './search-types';

const INITIAL_STATE = {
    query: '',
    sort: {sortBy: "", orderBy: ""},
    searchResults: { success: true, data: [] }
};

const SearchReducer = (state = INITIAL_STATE, { type, payload}) => {
    switch(type){
        case SEARCH_QUERY:
            return{
                ...state,
                query: payload
            };
        case SEARCH_SUCCESS:
            return{
                ...state,
                searchResults: payload
            };
        case UPDATE_INPUT:
            return {
                ...state,
                query: payload
            };
        default:
            return state;
    }
}

export default SearchReducer;