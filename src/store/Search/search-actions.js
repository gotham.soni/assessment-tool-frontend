import { SEARCH_SUCCESS, UPDATE_INPUT } from "./search-types";

import { getSearchResults } from "../../services/search-service";

export const asyncSearchQuery = (query, component, sort) => async dispatch => {
  let results = await getSearchResults(query, component, sort);
  if (query && results.data.length === 0) {
    results.data = null;
  }
  dispatch({ type: SEARCH_SUCCESS, payload: results });
};

export const updateField = event => async dispatch => {
  dispatch({ type: UPDATE_INPUT, payload: event.target.value });
};
