import React, { Component } from 'react';
// import './Search.css'
import { updateField, asyncSearchQuery } from '../../store/Search/search-actions'
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';

class Search extends Component {
    constructor(props) {
        super(props);

        this.state = {
            query: '',
            results: {},
            loading: false,
            message: '',
        }
        this.handleChange = this.handleChange.bind(this)
    }

    componentDidUpdate(oldprops) {
        var newprops = this.props;
        if (oldprops.searchQuery !== newprops.searchQuery || oldprops.sort !== newprops.sort){
            this.props.asyncSearchQuery(newprops.searchQuery, newprops.comp, newprops.sort)
        }
    }

    handleChange(event) {
        this.props.updateField(event);
        this.props.setSearchQuery(event.target.value)
    }


    render() {

        const useStyles = makeStyles(theme => ({
            textField: {
                marginLeft: theme.spacing(1),
                marginRight: theme.spacing(1),
                width: 200,
                height: 200
            },

        }));
        return (
            <div className="container">
                <label className="search-label" htmlFor="search-input">
                    <Input
                        value={this.props.searchQuery}
                        id="search-input"
                        label="Search ..."
                        type="search"
                        placeholder="Search..."
                        className={useStyles.textField}
                        onChange={(e) => this.handleChange(e)}
                        
                    />
                </label>
            </div>
        )
    }
}

Search.propTypes = {

}

const mapStateToProps = (state) => ({
    searchQuery: state.search.query,
    searchResults: state.search.searchResults.data
});

export default connect(mapStateToProps, { updateField, asyncSearchQuery })(Search);