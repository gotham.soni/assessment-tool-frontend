import React, { Component } from "react";
// import Dialog from '@material-ui/core/Dialog';
// import DialogContent from '@material-ui/core/DialogContent';
// import DialogTitle from '@material-ui/core/DialogTitle';
import { Dialog } from "primereact/dialog";

import ModuleForm from "./ModuleForm";
import { isNewModule } from "../../store/Modules/modules-actions";
import { connect } from "react-redux";

class ModuleFormDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: this.props.open
    };
    this.onShow = this.onShow.bind(this);
    this.onHide = this.onHide.bind(this);
  }

  componentDidUpdate(oldprops) {
    const newprops = this.props;
    if (oldprops.visible !== newprops.visible) {
      this.setState({ visible: newprops.visible });
    }
  }

  onShow() {
    this.setState({ visible: true });
  }

  onHide() {
    this.props.hideDialog();
  }

  render() {
    const moduleForm = isNewModule ? {} : this.props;

    return (
      <div>
        <Dialog
          header="Module"
          visible={this.state.visible}
          style={{ maxWidth: "80vw", height: "50vw", overflow: "auto" }}
          onHide={this.onHide}
          onShow={this.onShow}
          maximizable={true}
        >
          <ModuleForm
            moduleForm={moduleForm}
            visible={this.state.visible}
            hideDialog={this.onHide}
          />
        </Dialog>
      </div>
    );
  }
}

export default connect(null, { isNewModule })(ModuleFormDialog);
