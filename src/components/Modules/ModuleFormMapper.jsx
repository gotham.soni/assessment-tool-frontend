export function moduleMapper(inputData){
    var moduleObject = {};

    moduleObject.name = inputData.name;
    moduleObject.dateCreated = inputData.dateCreated;
    moduleObject.description = inputData.description;
    moduleObject.status = inputData.status;
    moduleObject.type = inputData.type;
    moduleObject.subType = inputData.subType;
    moduleObject.services = inputData.services;
    moduleObject.operations = inputData.operations;
    moduleObject.hardware = inputData.hardware;
    moduleObject.stack = inputData.stack;
    moduleObject.assessments = inputData.assessments;
    moduleObject.consumer = inputData.consumer;
    moduleObject.provider = inputData.provider;
    moduleObject.notes = inputData.notes;

    return moduleObject;
}