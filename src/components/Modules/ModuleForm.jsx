import React from "react";
import { Field, reduxForm, getFormValues } from "redux-form";
import { Component } from "react";
import { connect } from "react-redux";
import "primereact/resources/themes/nova-light/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import { Button } from "primereact/button";
import {
  asyncPostModuleForm,
  asyncLoadModuleToUpdate,
  asyncUpdateModuleForm,
  asyncFetchModules,
  asyncGetAssessmentsForModules
} from "../../store/Modules/modules-actions";
import { asyncFetchAssessmentForms } from "../../store/DashboardDataTable/dashboardDataTables-actions";
import { moduleMapper } from "./ModuleFormMapper";
import { withRouter } from "react-router-dom";
import { type, subType, stack, moduleStatus } from "../Common/FieldValues";
import Paper from "@material-ui/core/Paper";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import Link from "@material-ui/core/Link";
import { makeStyles } from "@material-ui/core/styles";
import {
  renderInputAreaField,
  renderDropdownField,
  renderField,
  renderCalendarField
} from "../Common/RenderFields";

const renderChips = ({
  input,
  label,
  moduleValues,
  options,
  name,
  meta: { touched, error, warning }
}) => {
  const filteredOptions = options.filter(option => {
    return option.modules.includes(moduleValues.name);
  });
  return (
    <div>
      <div className={"grid-container"}>
        <div className={"theme_number"}>
          <label> {label} </label>
        </div>
        <div className={"theme_number_input"}>
          {filteredOptions.length ? (
            filteredOptions.map((option, i) => {
              return (
                <div
                  key={i}
                  style={{ width: "90px", float: "left", display: "inline" }}
                >
                  <Paper>{option.label}</Paper>
                </div>
              );
            })
          ) : (
            <div>
              <i>There are no assessments associated with this module.</i>
            </div>
          )}
          {touched &&
            ((error && <span>{error}</span>) ||
              (warning && <span>{warning}</span>))}
        </div>
      </div>
    </div>
  );
};

class ModuleForm extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    asyncFetchAssessmentForms();
  }

  componentDidUpdate() {
    this.props.asyncGetAssessmentsForModules();
  }

  handleSubmit(e) {
    e.preventDefault();
    const newModule = moduleMapper(this.props.moduleValues);

    try {
      if (this.props.moduleToUpdate._id) {
        const moduleId = this.props.moduleToUpdate._id;
        this.props.asyncUpdateModuleForm(newModule, moduleId);
        this.props.asyncFetchModules();
      } else {
        this.props.asyncPostModuleForm(newModule);
        this.props.asyncFetchModules();
        this.props.hideDialog();
      }
    } catch (error) {
      console.log(error);
    }

    try {
      this.props.history.push("/modules/");
    } catch (e) {
      console.log(e);
    }
  }

  render() {
    const useStyles = makeStyles(theme => ({
      root: {
        flexGrow: 1,
        width: "100%",
        justifyContent: "center",
        flexWrap: "wrap"
      },
      paper: {
        padding: theme.spacing(1, 2)
      }
    }));

    return (
      <div style={{ transform: "scale(1.0)", transformOrigin: "0 0" }}>
        <div style={{ marginTop: "10px", marginBottom: "20px" }}>
          <Paper elevation={0} className={useStyles.paper}>
            <Breadcrumbs aria-label="breadcrumb">
              <Link color="inherit" href="/modules">
                Module Dashboard
              </Link>
              <Link color="inherit" href="/module-form">
                Create Module
              </Link>
            </Breadcrumbs>
          </Paper>
        </div>
        <h3 style={{ marginLeft: "20px", marginTop: "10px" }}>Modules</h3>

        <form>
          <Field name="name" type="text" component={renderField} label="Name" />
          <Field
            name="dateCreated"
            type="text"
            component={renderCalendarField}
            label="Date Created "
          />
          <Field
            name="description"
            type="text"
            component={renderInputAreaField}
            label="Description"
            width={50}
          />
          <Field
            name="status"
            type="text"
            component={renderDropdownField}
            options={moduleStatus}
            label="Status"
          />
          <Field
            name="type"
            type="text"
            component={renderDropdownField}
            options={type}
            label="Type"
          />
          <Field
            name="subType"
            type="text"
            component={renderDropdownField}
            options={subType}
            label="Sub Type"
          />
          <Field
            name="services"
            type="text"
            component={renderField}
            estimateValue={this.props.estimateValue}
            label="Services"
          />
          <Field
            name="operations"
            type="text"
            component={renderField}
            label="Operations "
          />
          <Field
            name="hardware"
            type="text"
            component={renderField}
            label="Hardware "
          />
          <Field
            name="stack"
            type="text"
            component={renderDropdownField}
            options={stack}
            label="Stack "
          />
          <Field
            name="consumer"
            type="text"
            component={renderField}
            label="Consumer "
          />
          <Field
            name="provider"
            type="text"
            component={renderField}
            label="Provider "
          />
          <Field
            name="notes"
            type="text"
            component={renderInputAreaField}
            label="Notes"
            width={50}
          />
          <Field
            name="assessments"
            component={renderChips}
            options={this.props.assessments_for_modules}
            moduleValues={this.props.moduleValues}
            label="Assessments "
          />
          <div style={{ margin: "20px", float: "right" }}>
            <Button
              label="Submit"
              icon="pi pi-check"
              onClick={this.handleSubmit}
            />
            <Button
              label="Cancel"
              icon="pi pi-times"
              onClick={this.props.hideDialog}
              className="p-button-secondary"
            />
          </div>
          <br />
          <br />
          <br />
          <br />
          {/* <div style={{ float: 'right' }}>
                        <button type="button" disabled={pristine || submitting} onClick={reset}>Clear All</button>
                    </div> */}
        </form>
      </div>
    );
  }
}

ModuleForm = reduxForm({
  form: "Modules",
  enableReinitialize: true,
  destroyOnUnmount: false
})(ModuleForm);

ModuleForm = connect(
  state => ({
    forms: state.dashboardDataTable.items,
    moduleToUpdate: state.modules.module,
    initialValues: state.modules.module, // pull initial values from account reducer
    moduleValues: getFormValues("Modules")(state),
    assessments_for_modules: state.modules.assessments_for_modules
  }),
  {
    asyncPostModuleForm,
    asyncLoadModuleToUpdate,
    asyncUpdateModuleForm,
    asyncFetchModules,
    asyncGetAssessmentsForModules
  }
)(ModuleForm);

export default withRouter(ModuleForm);
