import React, { Component } from 'react';
import { Toolbar } from 'primereact/toolbar';
import { Button } from 'primereact/button';
import ModuleFormDialog from './ModuleFormDialog'
import { asyncLoadModuleToUpdate, isNewModule } from '../../store/Modules/modules-actions';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
// import { asyncFetchAssessmentForms } from '../../store/DashboardDataTable/dashboardDataTables-actions';
import {withRouter} from 'react-router-dom';
class ModuleDashboardToolbar extends Component {

    constructor() {
        super();
        this.state = {
            moduleFormDialogVisible: false,
        };
        this.showModuleFormDialog = this.showModuleFormDialog.bind(this);
        this.hideModuleFormDialog = this.hideModuleFormDialog.bind(this);
    }


    showModuleFormDialog() {
        this.setState({moduleFormDialogVisible: true});
        this.props.asyncLoadModuleToUpdate({});
        this.props.isNewModule(true);
        this.props.history.push('/module-form/');

    }

    hideModuleFormDialog() {
        this.setState({moduleFormDialogVisible: false});
    }

    render() {

        return (
            <div>
                <div className="content-section implementation" >
                    <Toolbar style={{ backgroundColor: 'white', border: 'none' }}>
                    <ModuleFormDialog visible={this.state.moduleFormDialogVisible} hideDialog={this.hideModuleFormDialog}/>
                        <div className="p-toolbar-group-right" >
                            <Button label="New Module" icon="pi pi-plus" style={{ marginRight: '.25em', background: '#4B286D', borderColor: '#4B286D' }} onClick={this.showModuleFormDialog} />
                        </div>
                    </Toolbar>
                </div>
            </div>
        );
    }
}

//export default ModuleDashboardToolbar;

ModuleDashboardToolbar.propTypes = {
    asyncLoadModuleToUpdate: PropTypes.func.isRequired,
    modules: PropTypes.object
}

const mapStateToProps = (state) => ({
    moduleToUpdate: state.modules.module,
    
});

export default withRouter(connect(mapStateToProps, { asyncLoadModuleToUpdate, isNewModule })(ModuleDashboardToolbar));
