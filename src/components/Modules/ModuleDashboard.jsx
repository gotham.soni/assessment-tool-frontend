import React, { Component } from "react";
import "primereact/resources/themes/nova-light/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import "./ModuleDashboard.css";
import ModuleDashboardToolbar from "./ModuleDashboardToolbar";
import ModuleDashboardTable from "./ModuleDashboardTable";
import ModuleMostRecent from "./ModuleMostRecent";
import { PropTypes } from "prop-types";
import { connect } from "react-redux";
import { asyncFetchModules } from "../../store/Modules/modules-actions";
import DimpleDivider from "@tds/core-dimple-divider";

import Paper from "@material-ui/core/Paper";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import Link from "@material-ui/core/Link";
import { makeStyles } from "@material-ui/core/styles";

class ModuleDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
      cars1: [{}],
      selectedCars1: {},
      sort: { sortBy: false, order: "desc" }
    };

    this.handleChange = this.handleChange.bind(this);
    this.setSort = this.setSort.bind(this);
    this.props.asyncFetchModules();
  }

  componentDidMount() {
    this.setState({ visible: false });
  }

  handleChange(event, newValue) {
    //setValue(newValue);
    this.setState({ value: newValue });
  }

  async setSort(sortBy, orderBy) {
    await this.setState({ sort: { sortBy: sortBy, order: orderBy } });
    this.props.asyncFetchModules(this.state.sort);
  }

  render() {
    const modules = this.props;

    const useStyles = makeStyles(theme => ({
      root: {
        flexGrow: 1,
        width: "100%",
        justifyContent: "center",
        flexWrap: "wrap"
      },
      paper: {
        padding: theme.spacing(1, 2)
      }
    }));

    return (
      <div>
        <DimpleDivider />
        <h4 style={{ float: "left" }}> Recent Modules</h4>{" "}
        <ModuleDashboardToolbar />
        <ModuleMostRecent modules={modules} />
        <ModuleDashboardTable
          modules={modules}
          setSort={this.setSort}
          currentSort={this.state.sort}
        />
      </div>
    );
  }
}

//export default ModuleDashboard;

ModuleDashboard.propTypes = {
  asyncFetchModules: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  modules: state.modules.mods
});

export default connect(mapStateToProps, { asyncFetchModules })(ModuleDashboard);
