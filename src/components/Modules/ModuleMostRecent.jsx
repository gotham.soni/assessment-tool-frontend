import React, { Component } from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardActionArea from "@material-ui/core/CardActionArea";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import { PropTypes } from "prop-types";
import { connect } from "react-redux";
import ModuleFormDialog from "./ModuleFormDialog";
import Box from "@material-ui/core/Box";
import ButtonBase from "@material-ui/core/ButtonBase";
import {
  asyncLoadModuleToUpdate,
  isNewModule
} from "../../store/Modules/modules-actions";
import {
  getFormattedDate,
  getRecentItems
} from "../../utils/mostRecentHelpers";
import { withRouter } from "react-router-dom";

class ModuleMostRecent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ModuleFormDialogVisible: false,
      form: {}
    };
    this.getTableData = this.getTableData.bind(this);
  }

  showModuleFormDialog(form) {
    this.props.asyncLoadModuleToUpdate(form);

    this.props.isNewModule(false);
    this.props.history.push("/module-form/");
  }

  getTableData() {
    var data = [];
    try {
      if (this.props.modules.modules.data !== undefined) {
        return this.props.modules.modules.data;
      } else {
        return data;
      }
    } catch (e) {
      console.log(e);
    }
  }

  getCardColour = status => {
    switch (status) {
      case "ACTIVE":
        return "#66cc00";
      case "RETIRED":
        return "#6a6e74";
      default:
        return "#0d560a";
    }
  };

  render() {
    const useStyles = makeStyles({
      root: {
        flexGrow: 1,
        width: "100%"
      },
      card: {
        width: 275
      }
    });

    return (
      <div className={useStyles.root}>
        <ModuleFormDialog
          form={this.state.form}
          visible={this.state.ModuleFormDialogVisible}
          hideDialog={this.hideModuleFormDialog}
        />
        <Grid container item xs={12} spacing={3}>
          {getRecentItems(this.getTableData()).map(module => (
            <Grid key={module._id} item xs={3}>
              <Card
                className={useStyles.card}
                style={{ backgroundColor: this.getCardColour(module.status) }}
                border={1}
              >
                <CardActionArea component="div">
                  <Box
                    border={1.5}
                    justifyContent="left"
                    borderColor={this.getCardColour(module.status)}
                  >
                    <ButtonBase
                      onClick={() => this.showModuleFormDialog(module)}
                      style={{
                        textAlign: "left",
                        textTransform: "none",
                        width: "100%",
                        justifyContent: "left"
                      }}
                    >
                      <CardContent
                        style={{
                          color: module.status === "ON HOLD" ? "black" : "white"
                        }}
                      >
                        <Typography variant="subtitle2" gutterBottom>
                          Created {getFormattedDate(module._id, true, true)}
                        </Typography>

                        <Typography variant="h6" component="h2" gutterBottom>
                          {module.name}
                        </Typography>
                        <Typography variant="body2" gutterBottom>
                          <b>Type:</b> {module.type}
                          <br />
                          <b>Subtype:</b> {module.subType}
                        </Typography>

                        <Typography variant="body2" gutterBottom>
                          <b>Status:</b> <i>{module.status}</i>
                        </Typography>

                        <Typography variant="caption" gutterBottom>
                          {module.history ? (
                            <i>
                              {" "}
                              LAST UPDATED BY{" "}
                              {module.history[
                                module.history.length - 1
                              ].author.toUpperCase()}{" "}
                            </i>
                          ) : null}
                        </Typography>
                      </CardContent>
                    </ButtonBase>
                  </Box>
                </CardActionArea>
              </Card>
            </Grid>
          ))}
        </Grid>
      </div>
    );
  }
}

ModuleMostRecent.propTypes = {
  modules: PropTypes.object
};
const mapStateToProps = state => ({
  formToUpdate: state.dashboardDataTable.form
});
export default withRouter(
  connect(mapStateToProps, { asyncLoadModuleToUpdate, isNewModule })(
    ModuleMostRecent
  )
);
