import React, { Component } from "react";
import "primereact/resources/themes/nova-light/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import ModuleFormDialog from "./ModuleFormDialog";
import { PropTypes } from "prop-types";
import { connect } from "react-redux";
import {
  asyncLoadModuleToUpdate,
  isNewModule
} from "../../store/Modules/modules-actions";
import Search from "../Search/Search";
import { withRouter } from "react-router-dom";
import SortableTable from "../Common/SortableTable";

class ModuleDashboardTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      moduleFormDialogVisible: false,
      moduleForm: {},
      searchQuery: ""
    };

    this.showModuleFormDialog = this.showModuleFormDialog.bind(this);
    this.hideModuleFormDialog = this.hideModuleFormDialog.bind(this);
    this.handleSort = this.handleSort.bind(this);
    this.getTableData = this.getTableData.bind(this);
    this.setSearchQuery = this.setSearchQuery.bind(this);
  }

  showModuleFormDialog(moduleForm) {
    this.setState({ moduleForm: moduleForm });
    this.props.asyncLoadModuleToUpdate(moduleForm);
    this.props.isNewModule(false);
    this.props.history.push("/module-form/");
  }

  hideModuleFormDialog() {
    this.setState({ moduleFormDialogVisible: false });
  }

  getTableData() {
    var temp = [];
    if (this.props.modules.modules.data === undefined) {
      return temp;
    }

    try {
      if (this.state.searchQuery === "") {
        return this.props.modules.modules.data;
      } else {
        return this.props.searchResults;
      }
    } catch (e) {
      console.log(e);
    }
  }

  handleSort(column) {
    var { sortBy, order } = this.props.currentSort;
    if (sortBy === column) {
      order = order === "asc" ? "desc" : "asc";
    }
    this.setState({ order: order });
    this.props.setSort(column, order);
  }

  setSearchQuery(query) {
    this.setState({ searchQuery: query });
  }

  render() {
    const tableHeaders = [
      { title: "Name", id: "name" },
      { title: "Status", id: "status" },
      { title: "Sub-Type", id: "subType" },
      { title: "Consumer", id: "consumer" },
      { title: "Provider", id: "provider" },
      { title: "Stack", id: "stack" }
    ];

    return (
      <div>
        <hr />
        <ModuleFormDialog
          moduleForm={this.state.moduleForm}
          visible={this.state.moduleFormDialogVisible}
          hideDialog={this.hideModuleFormDialog}
        />
        <div>
          <div style={{ float: "left", display: "inline", width: "12%" }}>
            <h4 style={{ paddingTop: "17px" }}> All Modules</h4>
          </div>
          <div
            style={{
              float: "left",
              display: "inline",
              width: "49%",
              paddingTop: "17px"
            }}
          >
            <Search
              comp="module"
              sort={this.props.currentSort}
              setSearchQuery={this.setSearchQuery}
            />
          </div>
        </div>
        <br />
        <br />
        <br />
        <SortableTable
          tableHeaders={tableHeaders}
          tableData={this.getTableData()}
          currentSort={this.props.currentSort}
          handleSort={this.handleSort}
          showFormDialog={this.showModuleFormDialog}
        />
        <br />
      </div>
    );
  }
}

ModuleDashboardTable.propTypes = {
  asyncLoadModuleToUpdate: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  moduleToUpdate: state.modules.module,
  searchResults: state.search.searchResults.data
});

export default withRouter(
  connect(mapStateToProps, { asyncLoadModuleToUpdate, isNewModule })(
    ModuleDashboardTable
  )
);
