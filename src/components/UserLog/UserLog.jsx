import React, { Component } from "react";
import { Accordion, AccordionTab } from "primereact/accordion";
import { connect } from "react-redux";
import { getFormValues, destroy } from "redux-form";
import { PropTypes } from "prop-types";
import { getEstimates, getModules, getOthers } from "../Email/Content";

import "./UserLog.css";

class EmailContent extends Component {
  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
    this.createLog = this.createLog.bind(this);
    this.renderChanges = this.renderChanges.bind(this);
  }

  componentDidMount() {
    this.createLog();
  }

  renderChanges(history) {
    try {
      return (
        <div>
          {history.map((hist, index) => (
            <div key={index}>
              <Accordion>
                <AccordionTab header={hist._date}>
                  <ul>
                    <li>
                      <span style={{ color: "black" }}>
                        Date changed - {hist._date}
                      </span>
                    </li>
                    <li>
                      <span style={{ color: "black" }}>
                        Change By -&nbsp; {hist.author}{" "}
                      </span>
                    </li>
                    <li>
                      <span style={{ color: "black" }}>Changes </span>
                    </li>
                    {JSON.parse(hist.changes).old ? (
                      <div className="grid-container123">
                        <div className="grid-item123">
                          <ul>
                            <span style={{ color: "black" }}>BEFORE </span>
                            {Object.keys(JSON.parse(hist.changes).old).map(
                              (keyName, i) => (
                                <div key={i}>
                                  {keyName === "estimates" ? (
                                    <div>
                                      {getEstimates(
                                        JSON.parse(hist.changes).old[keyName]
                                      )}
                                      {getModules(
                                        JSON.parse(hist.changes).old[keyName]
                                      )}
                                      {getOthers(
                                        JSON.parse(hist.changes).old[keyName]
                                      )}
                                    </div>
                                  ) : (
                                    <li>
                                      <span style={{ color: "black" }}>
                                        {keyName} -{" "}
                                        {JSON.stringify(
                                          JSON.parse(hist.changes).old[keyName]
                                        )}
                                      </span>
                                    </li>
                                  )}
                                </div>
                              )
                            )}
                          </ul>
                        </div>
                        <div className="grid-item123">
                          <span style={{ color: "black" }}>AFTER </span>
                          <div>
                            {Object.keys(JSON.parse(hist.changes).new).map(
                              (keyName, i) => (
                                <div key={i}>
                                  {keyName === "estimates" ? (
                                    <div>
                                      {getEstimates(
                                        JSON.parse(hist.changes).new[keyName]
                                      )}
                                      {getModules(
                                        JSON.parse(hist.changes).new[keyName]
                                      )}
                                      {getOthers(
                                        JSON.parse(hist.changes).new[keyName]
                                      )}
                                    </div>
                                  ) : (
                                    <li>
                                      <span style={{ color: "black" }}>
                                        {keyName} -{" "}
                                        {JSON.stringify(
                                          JSON.parse(hist.changes).new[keyName]
                                        )}
                                      </span>
                                    </li>
                                  )}
                                </div>
                              )
                            )}
                          </div>
                        </div>
                      </div>
                    ) : (
                      <div></div>
                    )}
                  </ul>
                </AccordionTab>
              </Accordion>
            </div>
          ))}
        </div>
      );
    } catch (error) {}
  }

  createLog() {
    const { formToUpdate } = this.props;

    return (
      <div style={{ alignContent: "left" }}>
        <Accordion>
          <AccordionTab header="Change History">
            {this.renderChanges(formToUpdate.history)}
          </AccordionTab>
        </Accordion>
      </div>
    );
  }

  onClick(e) {
    e.preventDefault();
  }

  render() {
    return <div>{this.createLog()}</div>;
  }
}

//export default EmailContent;

EmailContent.propTypes = {
  basicProjectDetails: PropTypes.object,

  additionalInformation: PropTypes.object,

  contacts: PropTypes.object,

  estimates: PropTypes.object
};

const mapStateToProps = state => ({
  formToUpdate: state.dashboardDataTable.form,
  assessmentForm: getFormValues("AssessmentForm")(state),
  basicProjectDetails: getFormValues("BasicProjectDetails")(state),
  additionalInformation: getFormValues("AdditionalInformation")(state),
  contacts: getFormValues("Contacts")(state),
  estimates: getFormValues("Estimates")(state)
});

export default connect(mapStateToProps, { destroy })(EmailContent);
