export const type = [
  { label: "TOCP", value: "TOCP" },
  { label: "TMF", value: "TMF" },
  { label: "PubSub", value: "PubSub" },
  { label: "K2View", value: "K2View" },
  { label: "BPM", value: "BPM" }
];

export const subType = [
  { label: "Mediation", value: "Mediation" },
  { label: "Microflow", value: "Microflow" },
  { label: "Macroflow", value: "Macroflow" },
  { label: "Reverse Adaptor", value: "Reverse Adaptor" },
  { label: "Orchestration", value: "Orchestration" },

  { label: "Spec", value: "Spec" },
  { label: "Resources", value: "Resources" },

  { label: "TOPIC", value: "TOPIC" },
  { label: "Publisher", value: "Publisher" },
  { label: "Subscriber", value: "Subscriber" },

  { label: "User Task", value: "User Task" },
  { label: "System Task", value: "System Task" },
  { label: "UI", value: "UI" }
];

export const stack = [
  { label: "FIFA", value: "FIFA" },
  { label: "COMPASS", value: "COMPASS" },
  { label: "FIFA and COMPASS", value: "FIFA and COMPASS" }
];

export const moduleStatus = [
  { label: "ACTIVE", value: "ACTIVE" },
  { label: "RETIRED", value: "RETIRED" },
  { label: "NEW", value: "NEW" }
];

export const estimateStatus = [
  { label: "IN PROGRESS", value: "IN PROGRESS" },
  { label: "COMPLETED", value: "COMPLETED" }
];

export const granularityOptions = [
  { label: "+/- 100%", value: "+/- 100%" },
  { label: "50%", value: "+/- 50%" },
  { label: "25%", value: "+/- 25%" },
  { label: "10%", value: "+/- 10%" }
];

export const customer = [
  { label: "Residential", value: "Residential" },
  { label: "Business", value: "Business" }
];

export const status = [
  { label: "DELIVERED", value: "DELIVERED" },
  { label: "ASSESSMENT", value: "ASSESSMENT" },
  { label: "CANCELLED", value: "CANCELLED" },
  { label: "TESTING", value: "TESTING" },
  { label: "ANALYSIS/DESIGN", value: "ANALYSIS/DESIGN" },
  { label: "ON HOLD", value: "ON HOLD" },
  { label: "DEVELOPMENT", value: "DEVELOPMENT" }
];
