import React from "react";
import { InputText } from "primereact/inputtext";
import { InputTextarea } from "primereact/inputtextarea";
import { Dropdown } from "primereact/dropdown";
import { Calendar } from "primereact/calendar";

export const renderInputField = ({
  input,
  label,
  defaultValue,
  placeholder,
  name,
  width,
  meta: { touched, error, warning }
}) => (
  <div style={{ paddingBottom: "15px" }}>
    <div>
      <label> {label} </label>
    </div>
    <div className={"theme_number_input"}>
      <InputText
        {...input}
        name={name}
        defaultValue={defaultValue}
        style={{ height: "30px", width: { width } }}
        placeholder={placeholder}
      />
      {touched &&
        ((error && <span>{error}</span>) ||
          (warning && <span>{warning}</span>))}
    </div>
  </div>
);

export const renderField = ({
  input,
  label,
  type,
  name,
  meta: { touched, error }
}) => (
  <div style={{ paddingBottom: "5px" }}>
    <div>
      <label> {label} </label>
    </div>
    <div className={"theme_number_input"}>
      <InputText
        {...input}
        name={name}
        type={type}
        style={{ height: "30px", width: "300px" }}
        placeholder={label}
      />
      {touched && error && <span>{error}</span>}
    </div>
  </div>
);

export const renderInputAreaField = ({
  input,
  label,
  type,
  name,
  width,
  rows,
  meta: { touched, error, warning }
}) => (
  <div style={{ paddingBottom: "5px" }}>
    <div>
      <label> {label} </label>
    </div>
    <div className={"theme_number_input"}>
      <InputTextarea
        {...input}
        name={name}
        placeholder={label}
        type={type}
        rows={rows}
        cols={width}
        autoResize
      ></InputTextarea>

      {touched &&
        ((error && <span>{error}</span>) ||
          (warning && <span>{warning}</span>))}
    </div>
  </div>
);

export const renderDropdownField = ({
  input,
  label,
  placeholder,
  name,
  width,
  options,
  meta: { touched, error }
}) => (
  <div style={{ paddingBottom: "10px" }}>
    <div>
      <label> {label} </label>
    </div>
    <div className={"theme_number_input"}>
      <Dropdown
        {...input}
        name={name}
        options={options}
        placeholder={placeholder}
        style={{ height: "35px", width: width || "500px" }}
      />
      {touched && error && <span>{error}</span>}
    </div>
  </div>
);

export const renderCalendarField = ({
  input,
  label,
  type,
  name,
  meta: { touched, error }
}) => {
  const formatDate = input.value ? new Date(input.value) : false;
  return (
    <div>
      <div className={"grid-container"}>
        <div className={"theme_number"}>
          <label> {label} </label>
        </div>
        <div className={"theme_number_input"}>
          <Calendar
            {...input}
            dateFormat="yy-mm-dd"
            viewDate={formatDate}
            name={name}
            style={{ height: "30px", width: "290px" }}
            placeholder="Date"
          />
          {touched && error && <span>{error}</span>}
        </div>
      </div>
    </div>
  );
};
