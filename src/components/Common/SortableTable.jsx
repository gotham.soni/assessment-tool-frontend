import React from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import Paper from "@material-ui/core/Paper";
import _ from "lodash";
import {
  getFormattedDate,
  getRecentAuthor
} from "../../utils/mostRecentHelpers";

const SortableTable = ({
  tableHeaders,
  tableData,
  currentSort,
  handleSort,
  showFormDialog
}) => {
  var { sortBy, order } = currentSort;
  return (
    <Paper>
      <Table size="small">
        <TableHead style={{ backgroundColor: "#D8D8D8", size: "12" }}>
          <TableRow>
            {tableHeaders.map((header, index) => (
              <TableCell
                key={index}
                align={header.id === "name" ? "left" : "right"}
              >
                <TableSortLabel
                  active={header.id === sortBy}
                  direction={order}
                  onClick={() => handleSort(header.id)}
                >
                  {header.title}
                </TableSortLabel>
              </TableCell>
            ))}
          </TableRow>
        </TableHead>

        <TableBody>
          {tableData === null ? (
            <TableRow>
              <TableCell>No results found!</TableCell>
            </TableRow>
          ) : (
            tableData.map((form, index) => (
              <TableRow
                key={index + 1}
                data-item={form}
                onClick={() => showFormDialog(form)}
                className="tableRow"
              >
                {_.range(tableHeaders.length).map((row, i) => (
                  <TableCell
                    align={tableHeaders[i].id === "name" ? "left" : "right"}
                    key={i}
                  >
                    {tableHeaders[i].id === "_id"
                      ? getFormattedDate(form[tableHeaders[i].id], true, false)
                      : tableHeaders[i].id === "author"
                      ? getRecentAuthor(form)
                      : form[tableHeaders[i].id]}
                  </TableCell>
                ))}
              </TableRow>
            ))
          )}
        </TableBody>
      </Table>
    </Paper>
  );
};

export default SortableTable;
