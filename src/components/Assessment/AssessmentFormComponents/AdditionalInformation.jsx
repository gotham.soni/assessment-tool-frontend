import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import "primereact/resources/themes/nova-light/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import { connect } from "react-redux";
import { renderInputAreaField } from "../../Common/RenderFields";

class AdditionalInformation extends Component {
  render() {
    const { handleSubmit } = this.props;

    return (
      <div>
        <h2 style={{ marginLeft: "20px", marginTop: "10px" }}>
          Additional Information
        </h2>
        <form onSubmit={handleSubmit}>
          <Field
            name="summary"
            type="text"
            component={renderInputAreaField}
            label="Summary "
            width={80}
            rows={3}
          />
          <Field
            name="scope"
            type="text"
            component={renderInputAreaField}
            label="Scope "
            width={80}
            rows={3}
          />
          <Field
            name="benefits"
            type="text"
            component={renderInputAreaField}
            label="Key Benefits "
            width={80}
            rows={3}
          />
          <Field
            name="assumptions"
            type="text"
            component={renderInputAreaField}
            label="Assumptions "
            width={80}
            rows={3}
          />
        </form>
      </div>
    );
  }
}

AdditionalInformation = reduxForm({
  form: "AdditionalInformation",
  enableReinitialize: true,
  destroyOnUnmount: false
})(AdditionalInformation);

AdditionalInformation = connect(
  state => ({
    initialValues: state.dashboardDataTable.form // pull initial values from account reducer
  }) // bind account loading action creator
)(AdditionalInformation);

export default AdditionalInformation;
