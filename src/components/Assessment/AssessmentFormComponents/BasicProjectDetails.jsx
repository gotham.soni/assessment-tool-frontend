import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import { stack, customer, status } from "../../Common/FieldValues";
import { connect } from "react-redux";
import {
  renderInputField,
  renderDropdownField,
  renderCalendarField
} from "../../Common/RenderFields";

const required = value => (value ? undefined : "Required");
const number = value =>
  value && isNaN(Number(value)) ? "Must be a number" : undefined;

class BasicProjectDetails extends Component {
  render() {
    const { handleSubmit } = this.props;
    return (
      <div>
        <h2 style={{ marginLeft: "20px", marginTop: "10px" }}>
          Basic Project Details
        </h2>
        <form onSubmit={handleSubmit}>
          <Field
            name="name"
            type="text"
            component={renderInputField}
            label="Name "
            width={"500px"}
          />
          <Field
            name="themeNumber"
            type="number"
            component={renderInputField}
            label="Theme Number "
            validate={[number]}
            width={"500px"}
          />
          <Field
            name="itssNumber"
            type="number"
            component={renderInputField}
            label="ITSS Request Number "
            validate={[number]}
            width={"500px"}
          />
          <Field
            name="rppNumber"
            type="text"
            component={renderInputField}
            label="RPP Number "
            validate={[number]}
            width={"500px"}
          />
          <Field
            name="programName"
            type="text"
            component={renderInputField}
            width={"500px"}
            label="Program Name "
          />
          <Field
            name="stack"
            type="text"
            component={renderDropdownField}
            options={stack}
            label="Stack "
            validate={[required]}
          />
          <Field
            name="customerType"
            type="text"
            component={renderDropdownField}
            options={customer}
            label="Customer Type"
          />
          <Field
            name="status"
            type="text"
            component={renderDropdownField}
            options={status}
            label="Status "
            validate={[required]}
          />
          <Field
            name="startDate"
            type="text"
            component={renderCalendarField}
            label="Start Date "
          />
          <Field
            name="releaseDate"
            type="text"
            component={renderCalendarField}
            label="Release Date "
          />
        </form>
      </div>
    );
  }
}

BasicProjectDetails = reduxForm({
  form: "BasicProjectDetails",
  enableReinitialize: true,
  destroyOnUnmount: false
})(BasicProjectDetails);

BasicProjectDetails = connect(
  state => ({
    initialValues: state.dashboardDataTable.form // pull initial values from account reducer
  }) // bind account loading action creator
)(BasicProjectDetails);

export default BasicProjectDetails;
