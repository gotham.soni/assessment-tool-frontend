import React from "react";
import { Component } from "react";
import "primereact/resources/themes/nova-light/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableFooter from "@material-ui/core/TableFooter";
import Paper from "@material-ui/core/Paper";
import { InputText } from "primereact/inputtext";
import _ from "lodash";
import {
  asyncLoadFormToUpdate,
  isNewAssessment
} from "../../../store/DashboardDataTable/dashboardDataTables-actions";
import { PropTypes } from "prop-types";
import { connect } from "react-redux";
import { Button } from "@material-ui/core";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";

class ResourceProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      activeRow: -1,
      displayDWM: "days"
    };
  }

  componentDidMount() {
    this.populateData();
  }

  componentDidUpdate(oldProps) {
    if (oldProps !== this.props) {
      this.populateData();
    }
  }

  defaultData() {
    const defaultRoles = [
      "BSA",
      "Technology Architect",
      "System Analyst",
      "Developer Analyst",
      "Team Manager"
    ];
    const defaultRows = defaultRoles.map(role => {
      const row = {};
      row.columns = this.getColumns().map(column => {
        var retVal = {};
        if (column === "Role") {
          retVal.text = role;
        } else {
          retVal.text = "0";
        }
        retVal.column = column;
        return retVal;
      });
      row.id = Date.now() * _.uniqueId();
      return row;
    });
    this.setState({ data: defaultRows });
  }

  populateData() {
    let { resourceProfile } = this.props.formToUpdate;
    console.log(this.props.resourceProfile);
    if (this.state.data && this.state.data.length) {
      console.log("yup");
      resourceProfile = this.state.data;
    }
    if (resourceProfile && resourceProfile.length) {
      const columns = this.getColumns();
      console.log(columns);
      columns.forEach(column => {
        if (
          !resourceProfile.every(row =>
            row.columns.some(aColumn => aColumn.column === column)
          )
        ) {
          resourceProfile.forEach(row =>
            row.columns.push({ text: "", column: column })
          );
        }
      });
      this.setState({ data: resourceProfile });
      this.props.updateResourceProfile(resourceProfile);
    } else {
      this.defaultData();
    }
  }

  getColumns() {
    var columns = ["Role"];
    const startDate = new Date(this.props.startDate);
    const releaseDate = new Date(this.props.releaseDate);
    const monthNames = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"
    ];
    var fromYear = startDate.getFullYear();
    var toYear = releaseDate.getFullYear();
    var diffYear = 12 * (toYear - fromYear) + releaseDate.getMonth();

    for (var i = startDate.getMonth(); i <= diffYear; i++) {
      columns.push(monthNames[i % 12] + " " + Math.floor(fromYear + i / 12));
    }

    return columns;
  }

  addRow() {
    const data = [...this.state.data];
    const row = {};
    row.columns = this.getColumns().map(column => {
      return {
        text: "",
        column: column
      };
    });
    row.id = Date.now() * _.uniqueId();
    data.push(row);
    this.setState({ ...this.state, data });
    this.props.updateResourceProfile(data);
  }

  deleteRow(id) {
    const data = [...this.state.data];
    const filtered = data.filter(row => {
      return id !== row.id;
    });
    this.setState({ data: filtered });
    this.props.updateResourceProfile(filtered);
  }

  updateCell(text, rowId, columnId) {
    const data = [...this.state.data];
    let cellData = text;

    if (columnId !== "Role") {
      switch (this.state.displayDWM) {
        case "weeks":
          cellData = Number(cellData) * 5;
          break;
        case "months":
          cellData = Number(cellData) * 20;
          break;
      }
    }

    data
      .find(row => rowId === row.id)
      .columns.find(column => column.column === columnId).text = cellData;

    this.setState({ data: data });
    this.props.updateResourceProfile(data);
  }

  toggleRow(y) {
    if (this.state.activeRow !== y) this.setState({ activeRow: y });
  }

  getCellData(rowId, columnId) {
    const row = this.state.data.find(row => rowId === row.id);

    if (!row) {
      this.populateData();
    }
    let cellData;
    if (row.columns.find(column => columnId === column.column))
      cellData = row.columns.find(column => columnId === column.column).text;
    else cellData = "";
    if (columnId !== "Role") {
      if (cellData) {
        switch (this.state.displayDWM) {
          case "weeks":
            cellData = (Number(cellData) / 5).toFixed(2);
            break;
          case "months":
            cellData = (Number(cellData) / 20).toFixed(2);
            break;
        }
      } else {
        cellData = "0";
      }
      cellData = parseFloat(cellData);
    }
    return String(cellData);
  }

  toggleDWM() {
    const handleChange = (event, newValue) => {
      if (newValue === null) {
        newValue = "days";
      }
      this.setState({ displayDWM: newValue });
    };
    return (
      <div>
        <ToggleButtonGroup
          value={this.state.displayDWM}
          exclusive
          onChange={handleChange}
          size="small"
        >
          <ToggleButton value="days">D</ToggleButton>
          <ToggleButton value="weeks">W</ToggleButton>
          <ToggleButton value="months">M</ToggleButton>
        </ToggleButtonGroup>
      </div>
    );
  }

  sumRows() {
    let currVal = 0;

    this.state.data.map((row, y) =>
      this.getColumns().map((column, x) => {
        const toAdd = this.getCellData(row.id, column);
        if (!isNaN(toAdd)) currVal = currVal + Number(toAdd);
      })
    );
    currVal = parseFloat(currVal.toFixed(2));

    return currVal;
  }

  sumMonth(month) {
    let currVal = 0;

    this.state.data.map((row, y) =>
      this.getColumns().map((column, x) => {
        if (column === month) {
          const toAdd = this.getCellData(row.id, column);
          if (!isNaN(toAdd)) currVal = currVal + Number(toAdd);
        }
      })
    );
    currVal = parseFloat(currVal.toFixed(2));

    return currVal;
  }

  sortData() {
    var sortedData = this.state.data;
    if (this.state.activeRow === -1) {
      sortedData.sort((a, b) => {
        const aVal = a.columns[0].text;
        const bVal = b.columns[0].text;
        if (aVal === "" || aVal === null) return 1;
        if (bVal === "" || bVal === null) return -1;
        if (aVal === bVal) return 0;
        return aVal > bVal ? 1 : -1;
      });
      if (this.state.data !== sortedData) {
        this.setState({ data: sortedData });
      }
    }
  }

  renderRows() {
    this.sortData();
    return this.state.data.map((row, y) => (
      <TableRow
        key={y}
        onClick={() => {
          this.toggleRow(row.id);
        }}
        className="tableRow"
      >
        {this.getColumns().map((column, x) => {
          const isActive = this.state.activeRow === row.id;
          return (
            <TableCell
              key={x}
              align={column === "Role" ? "left" : "right"}
              style={{ border: "1px solid", borderColor: "#D8D8D8" }}
            >
              <div>
                {isActive ? (
                  <InputText
                    style={
                      column === "Role"
                        ? { width: "100%" }
                        : { textAlign: "right", width: "100%" }
                    }
                    keyfilter={column !== "Role" ? "int" : null}
                    defaultValue={this.getCellData(row.id, column)}
                    onChange={e =>
                      this.updateCell(e.target.value, row.id, column)
                    }
                  />
                ) : (
                  this.getCellData(row.id, column)
                )}
              </div>
            </TableCell>
          );
        })}
        <TableCell size="small" align="center">
          <Button
            size="small"
            style={{ width: 20, maxWidth: 20, minWidth: 20 }}
            onClick={() => this.deleteRow(row.id)}
          >
            <DeleteOutline />
          </Button>
        </TableCell>
      </TableRow>
    ));
  }

  render() {
    const columns = this.getColumns();
    const resourcesSum = this.sumRows();
    let totalResources = this.props.estimates.estimates[0].effort;
    switch (this.state.displayDWM) {
      case "weeks":
        totalResources = Number(totalResources) / 5;
        break;
      case "months":
        totalResources = Number(totalResources) / 20;
        break;
    }
    let percentageOfResources =
      (Number(resourcesSum) / Number(totalResources)) * 100;

    percentageOfResources = String(percentageOfResources.toFixed(2)) + "%";
    if (
      isNaN(percentageOfResources) ||
      percentageOfResources === Infinity ||
      percentageOfResources === undefined
    ) {
      percentageOfResources = 0;
    }
    return (
      <div>
        <h2 style={{ marginLeft: "20px", marginTop: "10px" }}>
          Resource Profile
        </h2>
        <ClickAwayListener onClickAway={() => this.toggleRow(-1)}>
          <Paper style={{ overflowX: "auto" }}>
            <Table size="small">
              <TableHead style={{ backgroundColor: "#D8D8D8", size: "12" }}>
                <TableRow>
                  {columns.map((header, index) => (
                    <TableCell
                      key={index}
                      align={header !== "Role" ? "right" : "left"}
                      style={
                        header !== "Role"
                          ? { width: 100, maxWidth: 100, minWidth: 100 }
                          : { width: 120, maxWidth: 120, minWidth: 120 }
                      }
                    >
                      <b>{header}</b>
                    </TableCell>
                  ))}
                  <TableCell
                    align="center"
                    style={{ width: 100, maxWidth: 100, minWidth: 100 }}
                  >
                    {this.toggleDWM()}
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody padding="none">{this.renderRows()}</TableBody>
              <TableRow className="tableRow">
                {columns.map((column, index) => (
                  <TableCell key={index} align="right">
                    <b>
                      {column === "Role"
                        ? "Total " +
                          String(this.sumRows()) +
                          " " +
                          this.state.displayDWM +
                          "   = "
                        : this.sumMonth(column)}
                    </b>
                  </TableCell>
                ))}
                <TableCell>
                  <div align="center">
                    <b>
                      {percentageOfResources +
                        " " +
                        this.state.displayDWM +
                        " assigned"}
                    </b>
                    {" (" +
                      String(resourcesSum) +
                      " / " +
                      String(totalResources) +
                      ")"}
                  </div>
                </TableCell>
              </TableRow>
            </Table>
          </Paper>
        </ClickAwayListener>

        <div align="center" onClick={() => this.addRow()}>
          <Paper>
            <Button onClick={() => this.addRow()} style={{ width: "100%" }}>
              Add Row
            </Button>
          </Paper>
        </div>
      </div>
    );
  }
}

ResourceProfile.propTypes = {
  asyncLoadFormToUpdate: PropTypes.func.isRequired,
  forms: PropTypes.object
};

const mapStateToProps = state => ({
  formToUpdate: state.dashboardDataTable.form,
  isNew: state.dashboardDataTable.isNewAssessment
});

export default connect(mapStateToProps, {
  asyncLoadFormToUpdate,
  isNewAssessment
})(ResourceProfile);
