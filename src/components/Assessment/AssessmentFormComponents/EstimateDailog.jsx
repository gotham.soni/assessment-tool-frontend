import React from "react";
import { Field, FieldArray, reduxForm, formValueSelector } from "redux-form";
import { Component } from "react";
import { connect } from "react-redux";
import "primereact/resources/themes/nova-light/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import { asyncGetModulesForAssessments } from "../../../store/DashboardDataTable/dashboardDataTables-actions";
import Estimate from "./Estimate";
import { Toolbar } from "primereact/toolbar";
import { Button } from "@material-ui/core";

const renderEstimates = ({
  fields,
  option,
  estimateValue,
  modulesForAssessment
}) => {
  return (
    <div align="left" style={{ width: "100%" }}>
      <Toolbar alignItems="center" justifyContent="center">
        <Button
          style={{ width: "100%", height: "100%" }}
          onClick={() => fields.push({})}
        >
          New Estimate
        </Button>
      </Toolbar>
      {fields.map((estimate, index) => {
        return (
          <FieldArray
            name="estimates"
            component={Estimate}
            estimateValue={estimateValue[index]}
            option={modulesForAssessment}
            index={index}
            estimate={estimate}
          />
        );
      })}
    </div>
  );
};

class Estimates extends Component {
  constructor(props) {
    super(props);
    this.calculateEstimateCost = this.calculateEstimateCost.bind(this);
    this.calculateEstimateEffort = this.calculateEstimateEffort.bind(this);
  }

  calculateEstimateCost(nextProps) {
    try {
      nextProps.estimateValue.forEach(estimate => {
        let totalModuleCost = 0;
        let totalOtherCost = 0;
        if (estimate.modules) {
          estimate.modules.forEach((emodule, index) => {
            if (emodule.effort) {
              if (
                !estimate.granularity ||
                estimate.granularity === "+/- 100%"
              ) {
                // +/-100% = 4K/week
                emodule.cost = (4000 * Number(emodule.effort)) / 5;
              } else {
                // other granularities: 713/day
                emodule.cost = 713 * Number(emodule.effort);
              }
              totalModuleCost = Number(totalModuleCost) + Number(emodule.cost);
            } else if (index === 0) {
              totalModuleCost = 0;
              emodule.cost = 0;
            }
          });
        }
        if (estimate.others) {
          estimate.others.forEach(other => {
            if (other.cost) {
              totalOtherCost = Number(totalOtherCost) + Number(other.cost);
            } else {
              totalOtherCost = 0;
            }
          });
        }
        estimate.cost = totalModuleCost + totalOtherCost;
      });
    } catch (e) {
      // console.log(e)
    }
  }

  calculateEstimateEffort(nextProps) {
    try {
      nextProps.estimateValue.forEach(estimate => {
        let totalModuleEffort = 0;
        let totalOtherEffort = 0;
        if (estimate.modules) {
          estimate.modules.forEach((emodule, index) => {
            if (emodule.effort) {
              totalModuleEffort =
                Number(totalModuleEffort) + Number(emodule.effort);
            } else if (index === 0) {
              totalModuleEffort = 0;
            }
          });
        }
        if (estimate.others) {
          estimate.others.forEach(other => {
            if (other.effort) {
              totalOtherEffort =
                Number(totalOtherEffort) + Number(other.effort);
            } else {
              totalOtherEffort = 0;
            }
          });
        }
        estimate.effort = totalModuleEffort + totalOtherEffort;
      });
    } catch (e) {
      // console.log(e)
    }
  }

  componentWillReceiveProps(nextProps) {
    this.calculateEstimateCost(nextProps);
    this.calculateEstimateEffort(nextProps);
  }

  render() {
    const { handleSubmit, pristine, reset, submitting } = this.props;
    console.log("higher", this.props.estimateValue);
    if (this.props.estimateValue) {
      return (
        <div>
          <h2 style={{ marginLeft: "20px", marginTop: "10px" }}>Estimates</h2>
          <form onSubmit={handleSubmit(handleSubmit)}>
            <FieldArray
              name="estimates"
              component={renderEstimates}
              estimateValue={this.props.estimateValue}
              option={this.props.modules_for_assessment}
              modulesForAssessment={this.props.modules_for_assessment}
            />
            <div style={{ float: "right" }}>
              <button
                type="button"
                disabled={pristine || submitting}
                onClick={reset}
              >
                Clear All
              </button>
            </div>
          </form>
        </div>
      );
    } else {
      return null;
    }
  }
}

Estimates = reduxForm({
  form: "Estimates",
  enableReinitialize: true,
  destroyOnUnmount: false
})(Estimates);

const selector = formValueSelector("Estimates");
Estimates = connect(
  state => {
    const estimateValue = selector(state, "estimates");
    return {
      initialValues: state.dashboardDataTable.form, // pull initial values from account reducer
      estimateValue,
      modules_for_assessment: state.dashboardDataTable.modules_for_assessment
    };
  },
  { asyncGetModulesForAssessments }
)(Estimates);

export default Estimates;
