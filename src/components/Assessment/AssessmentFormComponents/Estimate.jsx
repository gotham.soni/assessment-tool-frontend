import React from "react";
import { Field, FieldArray } from "redux-form";
import { Component } from "react";
import { Toolbar } from "primereact/toolbar";
import "primereact/resources/themes/nova-light/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import { Button } from "@material-ui/core";
import Heading from "@tds/core-heading";
import Grid from "@material-ui/core/Grid";
import {
  type,
  subType,
  estimateStatus,
  granularityOptions
} from "../../Common/FieldValues";
import Paper from "@material-ui/core/Paper";
import { Accordion, AccordionTab } from "primereact/accordion";
import {
  renderInputAreaField,
  renderDropdownField,
  renderField
} from "../../Common/RenderFields";
import Card from "@material-ui/core/Card";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import DeleteOutline from "@material-ui/icons/DeleteOutline";

var renderAutoCalculate = (label, value, isCost) => {
  if (!value) {
    value = 0;
  }
  if (isCost && value !== undefined) {
    value = "$" + value.toString();
  }
  return (
    <div style={{ width: "90px" }}>
      <Paper square style={{ textAlign: "center", backgroundColor: "#F7F7F8" }}>
        {value}
      </Paper>
    </div>
  );
};

const addModule = ({ fields }) => (
  <div align="center">
    <Paper>
      <Button
        onClick={() => fields.push({})}
        style={{ width: "100%", backgroundColor: "#D8D8D8" }}
      >
        Add Module
      </Button>
    </Paper>
  </div>
);

const renderHeader = (estimate, index, type) => {
  var name = type;
  if (estimate) {
    try {
      if (type === "Module" && estimate.modules[index].name) {
        name = estimate.modules[index].name;
      } else if (type === "Other" && estimate.others[index].name)
        name = estimate.others[index].name;
      else name = `${name} # ${index + 1}`;
    } catch {
      name = `${name} # ${index + 1}`;
    }
  }

  return <div>{name}</div>;
};

let renderEstimateModule = ({ fields, options, estimate, estimateIndex }) => {
  // TODO: Make sure "others" field is empty as well!
  console.log(estimate);
  return fields.map((emodule, index) => (
    <div key={index}>
      <Accordion>
        <AccordionTab header={renderHeader(estimate, index, "Module")}>
          <Grid container spacing={1}>
            <Grid container item xs={12} spacing={1}>
              <Grid item xs={4}>
                <Field
                  name={`${emodule}.name`}
                  type="text"
                  component={renderDropdownField}
                  label="Module Name"
                  options={options}
                  width={"150px"}
                />
              </Grid>
              <Grid item xs={4}>
                <Field
                  name={`${emodule}.type`}
                  type="text"
                  component={renderDropdownField}
                  label="Module Type"
                  options={type}
                  width={"150px"}
                />
              </Grid>
              <Grid item xs={4}>
                <Field
                  name={`${emodule}.subType`}
                  type="text"
                  component={renderDropdownField}
                  label="Module Subtype"
                  options={subType}
                  width={"150px"}
                />
              </Grid>
            </Grid>

            <Grid container item xs={12} spacing={1}>
              <Grid item xs={4}>
                <Field
                  name={`${emodule}.changes`}
                  type="text"
                  component={renderInputAreaField}
                  label="Module Changes"
                  width={40}
                  rows={1}
                />
              </Grid>
              <Grid item xs={4}>
                <Field
                  name={`${emodule}.effort`}
                  type="number"
                  component={renderField}
                  label="Module Effort"
                />
              </Grid>
              <Grid item xs={2}>
                <div style={{ paddingBottom: "2px" }}>
                  <label> Module Cost</label>
                </div>
                {renderAutoCalculate(
                  "Total Module Cost",
                  estimate.modules[index].cost,
                  true
                )}
              </Grid>
              <Grid item xs={2}>
                <div style={{ paddingTop: "20px" }}>
                  <Button
                    size="small"
                    style={{ width: 20, maxWidth: 20, minWidth: 20 }}
                    onClick={() => fields.remove(index)}
                  >
                    <DeleteOutline />
                  </Button>
                </div>
              </Grid>
            </Grid>
          </Grid>
          <br />
          <br />
        </AccordionTab>
      </Accordion>
    </div>
  ));
};

const addOther = ({ fields }) => (
  <div align="center">
    <Paper>
      <Button
        onClick={() => fields.push({})}
        style={{ width: "100%", backgroundColor: "#D8D8D8" }}
      >
        Add Other
      </Button>
    </Paper>
  </div>
);

const renderEstimateOthers = ({ fields, meta: { error }, estimateValue }) =>
  fields.map((eOther, index) => (
    <div key={index} className="content-section implementation">
      <Accordion>
        <AccordionTab header={renderHeader(estimateValue, index, "Other")}>
          <Grid container spacing={1}>
            <Grid container item xs={12} spacing={1}>
              <Grid item xs={4}>
                <Field
                  name={`${eOther}.name`}
                  type="text"
                  component={renderField}
                  label="Other Name"
                />
              </Grid>
              <Grid item xs={4}>
                <Field
                  name={`${eOther}.changes`}
                  type="text"
                  component={renderInputAreaField}
                  label="Other Changes"
                  width={40}
                  rows={1}
                />
              </Grid>
              <Grid item xs={4}>
                <Field
                  name={`${eOther}.assumptions`}
                  type="text"
                  component={renderInputAreaField}
                  label="Other Assumptions"
                  width={40}
                  rows={1}
                />
              </Grid>
            </Grid>
            <Grid container item xs={12} spacing={1}>
              <Grid item xs={4}>
                <Field
                  name={`${eOther}.effort`}
                  type="number"
                  component={renderField}
                  label="Other Effort"
                />
              </Grid>
              <Grid item xs={4}>
                <Field
                  name={`${eOther}.cost`}
                  type="number"
                  component={renderField}
                  label="Other Cost"
                />
              </Grid>
              <Grid item xs={4}>
                <div style={{ paddingTop: "20px" }}>
                  <Button
                    size="small"
                    style={{ width: 20, maxWidth: 20, minWidth: 20 }}
                    onClick={() => fields.remove(index)}
                  >
                    <DeleteOutline />
                  </Button>
                </div>
              </Grid>
            </Grid>
          </Grid>
          <br />
          <br />
        </AccordionTab>
      </Accordion>
    </div>
  ));

class Estimate extends Component {
  state = {
    granularity: null,
    status: null,
    displayDWM: "days",
    moduleDisplay: false
  };

  toggleGranularity = data => {
    const handleChange = (event, newValue) => {
      data.input.onChange(newValue);
      this.setState({ granularity: newValue });
    };

    const { options } = data;
    return (
      <div style={{ paddingBottom: "5px" }}>
        <ToggleButtonGroup
          {...data.input}
          exclusive
          onChange={handleChange}
          size="small"
        >
          {options.map(option => {
            return (
              <ToggleButton
                style={option.label === "+/- 100%" ? { width: "75px" } : null}
                value={option.value}
              >
                {option.label}
              </ToggleButton>
            );
          })}
        </ToggleButtonGroup>
      </div>
    );
  };

  toggleStatus = data => {
    const handleChange = (event, newValue) => {
      data.input.onChange(newValue);
      this.setState({ status: newValue });
    };
    const { options } = data;

    return (
      <div style={{ paddingBottom: "5px", width: "120px" }}>
        <ToggleButtonGroup
          value={this.state.granularity}
          exclusive
          onChange={handleChange}
          size="small"
        >
          {options.map(option => {
            return (
              <ToggleButton
                style={
                  option.label === "IN PROGRESS"
                    ? { fontSize: "13px", width: "113px" }
                    : null
                }
                value={option.value}
              >
                {option.label}
              </ToggleButton>
            );
          })}
        </ToggleButtonGroup>
      </div>
    );
  };

  toggleModuleDisplay() {
    const curr = this.state.moduleDisplay;
    if (curr === false) {
      this.setState({ moduleDisplay: true });
    } else {
      this.setState({ moduleDisplay: false });
    }
  }

  toggleDWM() {
    const handleChange = (event, newValue) => {
      this.setState({ displayDWM: newValue });
    };
    return (
      <div>
        <ToggleButtonGroup
          value={this.state.displayDWM}
          exclusive
          onChange={handleChange}
          size="small"
        >
          <ToggleButton value="days">Days</ToggleButton>
          <ToggleButton value="weeks">Weeks</ToggleButton>
          <ToggleButton value="months">Months</ToggleButton>
        </ToggleButtonGroup>
      </div>
    );
  }

  render() {
    const { fields, option, estimateValue, index, estimate } = this.props;
    if (estimateValue) {
      if (
        estimateValue.granularity &&
        this.state.granularity !== estimateValue.granularity
      )
        this.setState({ granularity: estimateValue.granularity });
      if (estimateValue.status && this.state.status !== estimateValue.status)
        this.setState({ status: estimateValue.status });
    }
    console.log("estimateValue", estimateValue, index);
    return (
      <Card
        key={index}
        align="left"
        style={{
          width: "100%",
          border: "1px solid",
          flex: 1,
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <Grid container spacing={4}>
          <Grid item xs={2}>
            <div
              style={{
                paddingTop: "10px",
                paddingLeft: "15px",
                paddingBottom: "15px"
              }}
            >
              <div>
                <div
                  style={{
                    display: "inline-block",
                    float: "left",
                    paddingRight: "10px",
                    paddingLeft: "5px"
                  }}
                >
                  <Heading level="h2">Estimate #{index + 1}</Heading>
                </div>

                <div
                  style={{
                    paddingTop: "5px",
                    opacity: "25%",
                    paddingBottom: "7px"
                  }}
                >
                  <Button
                    size="small"
                    style={{ width: 20, maxWidth: 20, minWidth: 20 }}
                    onClick={() => fields.remove(index)}
                  >
                    <DeleteOutline />
                  </Button>
                </div>
              </div>

              {estimateValue.status ? (
                <div>
                  <Field
                    name={`${estimate}.status`}
                    type="text"
                    component={data => {
                      console.log(data.input.value);
                      return (
                        <div
                          align="center"
                          style={{
                            width: "150px",
                            paddingBottom: "5px"
                          }}
                        >
                          <Paper>
                            <Button
                              onClick={() => {
                                data.input.onChange(null);
                              }}
                              style={{
                                width: "100%",
                                backgroundColor: "#2A2C2E",
                                color: "white"
                              }}
                            >
                              {data.input.value}
                            </Button>
                          </Paper>
                        </div>
                      );
                    }}
                    options={granularityOptions}
                  />
                </div>
              ) : (
                <Field
                  name={`${estimate}.status`}
                  type="text"
                  component={this.toggleStatus}
                  options={estimateStatus}
                  label="Status "
                  width="120px"
                />
              )}

              {estimateValue.granularity ? (
                <div>
                  <Field
                    name={`${estimate}.granularity`}
                    type="text"
                    component={data => {
                      console.log(data.input.value);
                      return (
                        <div
                          align="center"
                          style={{ width: "200px", paddingBottom: "5px" }}
                        >
                          <Paper>
                            <Button
                              onClick={() => {
                                data.input.onChange(null);
                              }}
                              style={{
                                width: "100%",
                                backgroundColor: "#54595F",
                                color: "white"
                              }}
                            >
                              {data.input.value} {" Granularity"}
                            </Button>
                          </Paper>
                        </div>
                      );
                    }}
                    options={granularityOptions}
                  />
                </div>
              ) : (
                <Field
                  name={`${estimate}.granularity`}
                  type="text"
                  component={this.toggleGranularity}
                  options={granularityOptions}
                  label="Granularity "
                  width="100px"
                />
              )}
              {this.state.displayDWM ? (
                <div align="center" style={{ width: "200px" }}>
                  <Paper>
                    <Button
                      style={{
                        width: "100%",
                        backgroundColor: "#71757B",
                        color: "white"
                      }}
                      onClick={() => this.setState({ displayDWM: null })}
                    >
                      {"Total Effort = "} <br />
                      {estimateValue.effort + " " + this.state.displayDWM}
                      &nbsp;
                      {"($" + estimateValue.cost + ")"}
                    </Button>
                  </Paper>
                </div>
              ) : (
                this.toggleDWM()
              )}
            </div>
          </Grid>

          <Grid item xs={10}>
            <div
              style={{
                paddingTop: "5px",
                paddingRight: "15px",
                paddingBottom: "5px"
              }}
            >
              <div>
                <div
                  style={{
                    display: "inline-block",
                    float: "left",
                    width: "50%"
                  }}
                >
                  <FieldArray
                    name={`${estimate}.modules`}
                    component={addModule}
                  />
                </div>
                <div
                  style={{
                    display: "inline-block",
                    float: "left",
                    width: "50%"
                  }}
                >
                  <FieldArray
                    name={`${estimate}.others`}
                    component={addOther}
                  />
                </div>
              </div>
              <div style={{ width: "100%", paddingTop: "37px" }}>
                <Paper>
                  <FieldArray
                    name={`${estimate}.modules`}
                    component={renderEstimateModule}
                    estimate={estimateValue}
                    estimateIndex={index}
                    options={option}
                  />
                  <FieldArray
                    name={`${estimate}.others`}
                    component={renderEstimateOthers}
                    estimateIndex={index}
                    estimateValue={estimateValue}
                  />
                </Paper>
              </div>
            </div>
          </Grid>
        </Grid>
      </Card>
    );
  }
}

export default Estimate;
