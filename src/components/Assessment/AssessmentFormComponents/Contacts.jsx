import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import "primereact/resources/themes/nova-light/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import { connect } from "react-redux";
import { renderInputField } from "../../Common/RenderFields";

class Contacts extends Component {
  render() {
    const { handleSubmit } = this.props;

    return (
      <div>
        <h2 style={{ marginLeft: "20px", marginTop: "10px" }}>Key People</h2>
        <form onSubmit={handleSubmit}>
          <Field
            name="projectManager"
            type="text"
            component={renderInputField}
            label="Project Manager "
            width={"300px"}
          />
          <Field
            name="e2eSolutionsArchitect"
            type="text"
            component={renderInputField}
            label="E2E Solutions Architect "
            width={"300px"}
          />
          <Field
            name="e2eBsa"
            type="text"
            component={renderInputField}
            label="E2E BSA "
            width={"300px"}
          />
        </form>
      </div>
    );
  }
}

Contacts = reduxForm({
  form: "Contacts",
  enableReinitialize: true,
  destroyOnUnmount: false
})(Contacts);

Contacts = connect(
  state => ({
    initialValues: state.dashboardDataTable.form // pull initial values from account reducer
  }) // bind account loading action creator
)(Contacts);

export default Contacts;
