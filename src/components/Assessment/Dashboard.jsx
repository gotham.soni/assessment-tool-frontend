import React, { Component } from "react";
import "primereact/resources/themes/nova-light/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import { makeStyles } from "@material-ui/core/styles";
import { PropTypes } from "prop-types";
import { connect } from "react-redux";
import { asyncFetchAssessmentForms } from "../../store/DashboardDataTable/dashboardDataTables-actions";
import DashboardDataTable from "./DashboardComponents/DashboardDataTable";
import DashboardMostRecent from "./DashboardComponents/DashboardMostRecent";
import AssessmentFormDialog from "./AssessmentFormDialog";
import Toolbar from "./DashboardComponents/DashboardToolbar";
import { asyncFetchModules } from "../../store/Modules/modules-actions";
import DimpleDivider from "@tds/core-dimple-divider";
import Paper from "@material-ui/core/Paper";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import Link from "@material-ui/core/Link";

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // EditorDialogVisible: false,
      AssessmentFormDialogVisible: false,
      sort: { sortBy: false, order: "asc" }
    };
    this.showDialog = this.showDialog.bind(this);
    this.hideDialog = this.hideDialog.bind(this);
    this.setSort = this.setSort.bind(this);
    this.props.asyncFetchAssessmentForms(this.state.sort);
  }

  componentDidMount() {
    this.setState({ visible: false });
    // this.props.asyncFetchAssessmentForms();
    return <DashboardDataTable />;
  }

  componentDidUpdate(oldprops) {
    const newprops = this.props;
    if (oldprops.EditorDialogVisible !== newprops.EditorDialogVisible) {
      this.setState({ EditorDialogVisible: newprops.EditorDialogVisible });
    }
    if (
      oldprops.AssessmentFormDialogVisible !==
      newprops.AssessmentFormDialogVisible
    ) {
      this.setState({
        AssessmentFormDialogVisible: newprops.AssessmentFormDialogVisible
      });
    }
    return <DashboardDataTable />;
  }

  showDialog() {
    this.setState({ EditorDialogVisible: true });
  }

  hideDialog() {
    this.setState({ EditorDialogVisible: false });
  }

  async setSort(sortBy, orderBy) {
    await this.setState({ sort: { sortBy: sortBy, order: orderBy } });
    this.props.asyncFetchAssessmentForms(this.state.sort);
  }

  render() {
    const useStyles = makeStyles(theme => ({
      root: {
        flexGrow: 1,
        width: "100%",
        justifyContent: "center",
        flexWrap: "wrap"
      },
      paper: {
        padding: theme.spacing(1, 2)
      }
    }));

    const { forms } = this.props;

    return (
      <div className={useStyles.root}>
        <DimpleDivider />
        <AssessmentFormDialog
          forms={forms}
          visible={this.state.AssessmentFormDialogVisible}
          hideDialog={this.hideAssessmentFormDialog}
        />
        <h4 style={{ float: "left" }}> Recent Projects</h4>{" "}
        <Toolbar forms={forms} />
        <DashboardMostRecent forms={forms} />
        <DashboardDataTable
          forms={forms}
          setSort={this.setSort}
          currentSort={this.state.sort}
        />
      </div>
    );
  }
}

Dashboard.propTypes = {
  asyncFetchAssessmentForms: PropTypes.func.isRequired,
  forms: PropTypes.object
};

const mapStateToProps = state => ({
  forms: state.dashboardDataTable.items
});

export default connect(mapStateToProps, {
  asyncFetchAssessmentForms,
  asyncFetchModules
})(Dashboard);
