import React, { Component } from "react";
import { Growl } from "primereact/growl";
import "primereact/resources/themes/nova-light/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import "./AssessmentForm.css";
import { Button } from "primereact/button";
import BasicProjectDetails from "./AssessmentFormComponents/BasicProjectDetails";
import AdditionalInformation from "./AssessmentFormComponents/AdditionalInformation";
import Contacts from "./AssessmentFormComponents/Contacts";
import ResourceProfile from "./AssessmentFormComponents/ResourceProfileT";
import EstimateDialog from "./AssessmentFormComponents/EstimateDailog";
import {
  asyncPostForm,
  asyncUpdateForm,
  asyncFetchAssessmentForms,
  asyncGetModulesForAssessments
} from "../../store/DashboardDataTable/dashboardDataTables-actions";
import { PropTypes } from "prop-types";
import { connect } from "react-redux";
import { getFormValues, destroy } from "redux-form";
import { Route } from "react-router-dom";
import { Messages } from "primereact/messages";
import Dashboard from "./Dashboard";
import UserLog from "../UserLog/UserLog";

import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import Link from "@material-ui/core/Link";
import { withRouter } from "react-router-dom";

import EmailContent from "../Email/EmailContent";

class AssessmentForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ResourceProfile: [],
      visible: this.props.visible
    };
    this.showSuccess = this.showSuccess.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.updateResourceProfile = this.updateResourceProfile.bind(this);
    this.returnToDashboard = this.returnToDashboard.bind(this);
    this.shouldRenderRPTable = this.shouldRenderRPTable.bind(this);
  }

  componentWillMount() {
    this.props.asyncGetModulesForAssessments();
  }
  updateResourceProfile(data) {
    this.setState({ ResourceProfile: data });
  }

  showSuccess() {
    this.messages.show({
      severity: "success",
      summary: "Success Message",
      detail: "Order submitted"
    });
  }

  shouldRenderRPTable(estimates) {
    if (estimates) {
      try {
        for (var estimate of estimates.estimates) {
          if (estimate.granularity !== "+/- 100%") {
            return true;
          }
        }
        return false;
      } catch (error) {
        // console.log(error)
      }
    }
  }

  returnToDashboard() {
    return (
      <div>
        <Route exact path={"/home"} component={Dashboard} />
      </div>
    );
  }

  handleSubmit(e) {
    e.preventDefault();

    const {
      basicProjectDetails,
      additionalInformation,
      contacts,
      estimates,
      auth
    } = this.props;

    var form = {};

    try {
      form.name = basicProjectDetails.name;
      form.themeNumber = basicProjectDetails.themeNumber;
      form.itssNumber = basicProjectDetails.itssNumber;
      form.rppNumber = basicProjectDetails.rppNumber;
      form.programName = basicProjectDetails.programName;
      form.stack = basicProjectDetails.stack;
      form.customerType = basicProjectDetails.customerType;
      form.status = basicProjectDetails.status;
      form.startDate = basicProjectDetails.startDate;
      form.releaseDate = basicProjectDetails.releaseDate;

      form.summary = additionalInformation.summary;
      form.scope = additionalInformation.scope;
      form.benefits = additionalInformation.benefits;
      form.assumptions = additionalInformation.assumptions;

      //"_comment3": "Contacts",
      form.projectManager = contacts.projectManager;
      form.e2eSolutionsArchitect = contacts.e2eSolutionsArchitect;
      form.e2eBsa = contacts.e2eBsa;

      //"_comment4": "Estimate",
      form.estimates = estimates.estimates;
      //"_comment5": "Resource Profile",
      form.resourceProfile = this.state.ResourceProfile;

      form.author = auth.name;
    } catch (error) {
      console.log(error);
    }

    try {
      if (this.props.formToUpdate._id) {
        const formId = this.props.formToUpdate._id;
        this.props.asyncUpdateForm(form, formId);
        this.props.asyncFetchAssessmentForms();
      } else {
        this.props.asyncPostForm(form);
        this.growl.show({
          severity: "success",
          summary: "Success Message",
          detail: "Order submitted"
        });
        this.props.asyncFetchAssessmentForms();
      }
    } catch (error) {
      console.log(error);
    }

    try {
      this.props.history.push("/dashboard/");
    } catch (e) {
      console.log(e);
    }

    this.props.destroy(
      "BasicProjectDetails",
      "AdditionalInformation",
      "Contacts",
      "Estimates"
    );
  }

  render() {
    const formToUpdate = this.props;
    console.log(this.props.estimates);
    return (
      <div>
        <Messages ref={el => (this.messages = el)} />
        <Growl ref={el => (this.growl = el)} />

        <BasicProjectDetails />
        <AdditionalInformation />
        <Contacts />
        <EstimateDialog />

        <br />

        {this.shouldRenderRPTable(this.props.estimates) ? (
          <ResourceProfile
            updateResourceProfile={this.updateResourceProfile}
            resourceProfile={this.state.resourceProfile}
            formToUpdate={formToUpdate}
            estimates={this.props.estimates}
            startDate={this.props.basicProjectDetails.startDate}
            releaseDate={this.props.basicProjectDetails.releaseDate}
          />
        ) : (
          <div></div>
        )}

        <br />
        <br />
        <br />

        <div style={{ margin: "20px", float: "right" }}>
          <Button
            label="Submit"
            icon="pi pi-check"
            onClick={this.handleSubmit}
            style={{ marginRight: "5px" }}
          />
          <Button
            label="Cancel"
            icon="pi pi-times"
            onClick={this.props.hideDialog}
            className="p-button-secondary"
          />
        </div>

        <br />
        <br />

        <h3 style={{ marginLeft: "20px", marginTop: "10px" }}>Email Content</h3>
        <EmailContent resourceProfile={this.state.ResourceProfile} />

        <h3 style={{ marginLeft: "20px", marginTop: "10px" }}>
          Change History
        </h3>
        <UserLog />

        <br />
        <br />
        <br />
        <br />
      </div>
    );
  }
}

AssessmentForm.propTypes = {
  asyncPostForm: PropTypes.func,
  asyncFetchAssessmentForms: PropTypes.func
};

const mapStateToProps = state => ({
  formToUpdate: state.dashboardDataTable.form,
  assessmentForm: getFormValues("AssessmentForm")(state),
  basicProjectDetails: getFormValues("BasicProjectDetails")(state),
  additionalInformation: getFormValues("AdditionalInformation")(state),
  contacts: getFormValues("Contacts")(state),
  estimates: getFormValues("Estimates")(state),
  resourceProfile: getFormValues("ResourceProfile")(state),
  auth: state.auth.user,
  modules: state.modules.mods,
  modules_for_assessment: state.dashboardDataTable.modules_for_assessment
});

export default withRouter(
  connect(mapStateToProps, {
    destroy,
    asyncPostForm,
    asyncUpdateForm,
    asyncFetchAssessmentForms,
    asyncGetModulesForAssessments
  })(AssessmentForm)
);
