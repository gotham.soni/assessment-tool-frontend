import React, { Component } from "react";
import "primereact/resources/themes/nova-light/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import "./AssessmentForm.css";
import { connect } from "react-redux";
import { destroy } from "redux-form";
import { Dialog } from "primereact/dialog";
import AssessmentForm from "./AssessmentForm";
import { isNewAssessment } from "../../store/DashboardDataTable/dashboardDataTables-actions";

class AssessmentFormDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: this.props.visible
    };
    this.onShow = this.onShow.bind(this);
    this.onHide = this.onHide.bind(this);
  }

  componentDidUpdate(oldprops) {
    const newprops = this.props;
    if (oldprops.visible !== newprops.visible) {
      this.setState({ visible: newprops.visible, forms: newprops.forms });
    }
  }

  onShow() {
    if (this.props.isNewAssessment) {
    }
    this.setState({ visible: true });
  }

  onHide() {
    this.props.hideDialog();
  }

  render() {
    return (
      <div>
        <Dialog
          header="Initial Assessment"
          visible={this.state.visible}
          style={{ maxWidth: "80vw", height: "50vw", overflow: "auto" }}
          onHide={this.onHide}
          onShow={this.onShow}
          maximizable={true}
        >
          <AssessmentForm
            visible={this.state.visible}
            hideDialog={this.onHide}
          />
        </Dialog>
      </div>
    );
  }
}

export default connect(null, { destroy, isNewAssessment })(
  AssessmentFormDialog
);
