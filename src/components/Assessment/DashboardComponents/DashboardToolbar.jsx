import React, { Component } from "react";

import "primereact/resources/themes/nova-light/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import { withRouter } from "react-router-dom";

import "../AssessmentForm.css";
import { Toolbar } from "primereact/toolbar";
import { Button } from "primereact/button";
import AssessmentFormDialog from "../AssessmentFormDialog";
import {
  asyncLoadFormToUpdate,
  isNewAssessment
} from "../../../store/DashboardDataTable/dashboardDataTables-actions";
import { PropTypes } from "prop-types";
import { connect } from "react-redux";

class DashboardToolbar extends Component {
  constructor() {
    super();
    this.state = {
      AssessmentFormDialogVisible: false
    };

    this.showAssessmentFormDialog = this.showAssessmentFormDialog.bind(this);
    this.hideAssessmentFormDialog = this.hideAssessmentFormDialog.bind(this);
  }

  showAssessmentFormDialog() {
    this.props.asyncLoadFormToUpdate({});
    this.props.isNewAssessment(true);
    this.props.history.push("/initial-assessment#/");
  }

  hideAssessmentFormDialog() {
    this.setState({ AssessmentFormDialogVisible: false });
  }

  render() {
    //const forms = this.props;
    return (
      <div>
        <div className="content-section implementation">
          <Toolbar style={{ backgroundColor: "white", border: "none" }}>
            {/* <AssessmentFormDialog visible={this.state.AssessmentFormDialogVisible} hideDialog={this.hideAssessmentFormDialog} /> */}
            <div className="p-toolbar-group-right">
              <Button
                label="New Assessment"
                icon="pi pi-plus"
                style={{
                  marginRight: ".25em",
                  background: "#4B286D",
                  borderColor: "#4B286D"
                }}
                onClick={this.showAssessmentFormDialog}
              />
            </div>
          </Toolbar>
        </div>
      </div>
    );
  }
}

DashboardToolbar.propTypes = {
  asyncLoadFormToUpdate: PropTypes.func.isRequired,
  forms: PropTypes.object
};

const mapStateToProps = state => ({
  formToUpdate: state.dashboardDataTable.form
});

export default withRouter(
  connect(mapStateToProps, { asyncLoadFormToUpdate, isNewAssessment })(
    DashboardToolbar
  )
);
