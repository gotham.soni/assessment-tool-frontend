import React, { Component } from "react";
import { PropTypes } from "prop-types";
import { connect } from "react-redux";
import {
  asyncLoadFormToUpdate,
  isNewAssessment
} from "../../../store/DashboardDataTable/dashboardDataTables-actions";
import Search from "../../Search/Search";
import { withRouter } from "react-router-dom";
import SortableTable from "../../Common/SortableTable";

class DashboardDataTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      AssessmentFormDialogVisible: false,
      form: {},
      searchQuery: ""
    };

    this.showAssessmentFormDialog = this.showAssessmentFormDialog.bind(this);
    this.hideAssessmentFormDialog = this.hideAssessmentFormDialog.bind(this);
    this.getTableData = this.getTableData.bind(this);
    this.handleSort = this.handleSort.bind(this);
    this.setSearchQuery = this.setSearchQuery.bind(this);
  }

  showAssessmentFormDialog(form) {
    this.setState({ form: form });
    this.props.asyncLoadFormToUpdate(form);
    this.props.isNewAssessment(false);
    this.props.history.push("/initial-assessment#/");
  }

  hideAssessmentFormDialog() {
    this.setState({ AssessmentFormDialogVisible: false });
  }

  getTableData() {
    var temp = [];
    if (
      this.props.forms.data === undefined ||
      this.props.searchResults === undefined
    ) {
      return temp;
    }
    try {
      if (this.state.searchQuery === "") {
        if (
          this.props.searchResults === undefined ||
          this.props.forms.data === undefined
        ) {
          return temp;
        }
        return this.props.forms.data;
      } else {
        if (
          this.props.searchResults === undefined ||
          this.props.forms.data === undefined
        ) {
          return temp;
        }
        return this.props.searchResults;
      }
    } catch (e) {
      console.log(e);
    }
  }

  handleSort(column) {
    var { sortBy, order } = this.props.currentSort;
    if (sortBy === column) {
      order = order === "asc" ? "desc" : "asc";
    }
    this.setState({ order: order });
    this.props.setSort(column, order);
  }

  setSearchQuery(query) {
    this.setState({ searchQuery: query });
  }

  render() {
    const tableHeaders = [
      { title: "Name", id: "name" },
      { title: "Theme Number", id: "themeNumber" },
      { title: "ITSS Number", id: "itssNumber" },
      { title: "Status", id: "status" },
      { title: "Date Created", id: "_id" },
      { title: "Last Updated By", id: "author" }
    ];

    return (
      <div>
        <hr />
        <div>
          <div style={{ float: "left", display: "inline", width: "10%" }}>
            <h4 style={{ paddingTop: "17px" }}> All Projects</h4>
          </div>
          <div
            style={{
              float: "left",
              display: "inline",
              width: "49%",
              paddingTop: "17px"
            }}
          >
            <Search
              comp="assessment"
              sort={this.props.currentSort}
              setSearchQuery={this.setSearchQuery}
            />
          </div>
        </div>
        <br />
        <br />
        <br />
        <SortableTable
          tableHeaders={tableHeaders}
          tableData={this.getTableData()}
          currentSort={this.props.currentSort}
          handleSort={this.handleSort}
          showFormDialog={this.showAssessmentFormDialog}
        />
      </div>
    );
  }
}

DashboardDataTable.propTypes = {
  asyncLoadFormToUpdate: PropTypes.func.isRequired,
  forms: PropTypes.object
};

const mapStateToProps = state => ({
  formToUpdate: state.dashboardDataTable.form,
  searchResults: state.search.searchResults.data
});

export default withRouter(
  connect(mapStateToProps, { asyncLoadFormToUpdate, isNewAssessment })(
    DashboardDataTable
  )
);
