import React, { Component } from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardActionArea from "@material-ui/core/CardActionArea";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import {
  asyncLoadFormToUpdate,
  isNewAssessment
} from "../../../store/DashboardDataTable/dashboardDataTables-actions";
import { PropTypes } from "prop-types";
import { connect } from "react-redux";
import AssessmentFormDialog from "../AssessmentFormDialog";
import Box from "@material-ui/core/Box";
import ButtonBase from "@material-ui/core/ButtonBase";
import {
  getFormattedDate,
  getRecentItems
} from "../../../utils/mostRecentHelpers";
import { withRouter } from "react-router-dom";

class DashboardMostRecent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      AssessmentFormDialogVisible: false,
      form: {}
    };
    this.showAssessmentFormDialog = this.showAssessmentFormDialog.bind(this);
    this.hideAssessmentFormDialog = this.hideAssessmentFormDialog.bind(this);
    this.getTableData = this.getTableData.bind(this);
  }

  showAssessmentFormDialog(form) {
    this.setState({ form: form });
    this.props.asyncLoadFormToUpdate(form);
    this.props.isNewAssessment(false);
    this.props.history.push("/initial-assessment#/");
  }

  hideAssessmentFormDialog() {
    this.setState({ AssessmentFormDialogVisible: false });
    this.forceUpdate();
  }

  getTableData() {
    var data = [];
    try {
      if (this.props.forms.data !== undefined) {
        return this.props.forms.data;
      } else {
        return data;
      }
    } catch (e) {
      console.log(e);
    }
  }

  getCardColour = status => {
    switch (status) {
      case "ASSESSMENT":
        return "rgba(102, 204, 0, 0.65)";
      case "ANALYSIS/DESIGN":
        return "rgba(102, 204, 0, 0.7)";
      case "DEVELOPMENT":
        return "rgba(102, 204, 0, 0.8)";
      case "TESTING":
        return "rgba(102, 204, 0, 0.85)";
      case "DELIVERED":
        return "rgba(102, 204, 0, 1)";
      case "ON HOLD":
        return "#ffff00";
      case "CANCELLED":
        return "#6a6e74";
      default:
        return "#0d560a";
    }
  };

  render() {
    const useStyles = makeStyles({
      root: {
        flexGrow: 1,
        width: "100%"
      },
      card: {
        width: 275
      }
    });

    return (
      <div className={useStyles.root}>
        <AssessmentFormDialog
          form={this.state.form}
          visible={this.state.AssessmentFormDialogVisible}
          hideDialog={this.hideAssessmentFormDialog}
        />
        <Grid container item xs={12} spacing={3}>
          {getRecentItems(this.getTableData()).map(form => (
            <Grid key={form._id} item xs={3}>
              <Card
                className={useStyles.card}
                style={{
                  backgroundColor: this.getCardColour(form.status),
                  boxShadow: "2px 2px 2px black"
                }}
                border={1}
              >
                <CardActionArea component="div">
                  <Box
                    border={1.5}
                    justifyContent="left"
                    borderColor={this.getCardColour(form.status)}
                  >
                    <ButtonBase
                      onClick={() => this.showAssessmentFormDialog(form)}
                      style={{
                        textAlign: "left",
                        textTransform: "none",
                        width: "100%",
                        justifyContent: "left"
                      }}
                    >
                      <CardContent
                        style={{
                          color: form.status === "ON HOLD" ? "black" : "white"
                        }}
                      >
                        <Typography variant="subtitle2" gutterBottom>
                          {form.history ? "Updated" : "Created"}{" "}
                          {form.history
                            ? getFormattedDate(
                                form.history[0]._date,
                                false,
                                true
                              )
                            : getFormattedDate(form._id, true, true)}
                        </Typography>

                        <Typography variant="h6" component="h2" gutterBottom>
                          {form.name}
                        </Typography>

                        <Typography variant="body2" gutterBottom>
                          <b>Theme:</b> {form.themeNumber}
                          &nbsp;&nbsp;&nbsp;
                          <b>RPP:</b> {form.rppNumber}
                          &nbsp;&nbsp;&nbsp;
                          <b>ITSS:</b> {form.itssNumber}
                        </Typography>

                        <Typography variant="body2" gutterBottom>
                          <b>Status:</b> <i>{form.status}</i>
                        </Typography>

                        <Typography variant="caption" gutterBottom>
                          {form.history ? (
                            <i>
                              {" "}
                              LAST UPDATED BY{" "}
                              {form.history[
                                form.history.length - 1
                              ].author.toUpperCase()}{" "}
                            </i>
                          ) : null}
                        </Typography>
                      </CardContent>
                    </ButtonBase>
                  </Box>
                </CardActionArea>
              </Card>
            </Grid>
          ))}
        </Grid>
      </div>
    );
  }
}

DashboardMostRecent.propTypes = {
  asyncLoadFormToUpdate: PropTypes.func.isRequired,
  forms: PropTypes.object
};
const mapStateToProps = state => ({
  formToUpdate: state.dashboardDataTable.form
});
export default withRouter(
  connect(mapStateToProps, { asyncLoadFormToUpdate, isNewAssessment })(
    DashboardMostRecent
  )
);
