export function assessmentFormMapper(
  basicProjectDetails,
  additionalInformation,
  contacts,
  estimates,
  resourceProfile,
  auth
) {
  var form = {};
  form.name = basicProjectDetails.name;
  form.themeNumber = basicProjectDetails.themeNumber;
  form.itssNumber = basicProjectDetails.itssNumber;
  form.rppNumber = basicProjectDetails.rppNumber;
  form.programName = basicProjectDetails.programName;
  form.stack = basicProjectDetails.stack;
  form.customerType = basicProjectDetails.customerType;
  form.status = basicProjectDetails.status;
  form.startDate = basicProjectDetails.startDate;
  form.releaseDate = basicProjectDetails.releaseDate;

  form.summary = additionalInformation.summary;
  form.scope = additionalInformation.scope;
  form.benefits = additionalInformation.benefits;
  form.assumptions = additionalInformation.assumptions;

  //"_comment3": "Contacts",
  form.projectManager = contacts.projectManager;
  form.e2eSolutionsArchitect = contacts.e2eSolutionsArchitect;
  form.e2eBsa = contacts.e2eBsa;

  //"_comment4": "Estimate",
  form.estimates = estimates.estimates;
  //"_comment5": "Resource Profile",
  form.resourceProfile = resourceProfile;

  form.author = auth.name;

  return form;
}
