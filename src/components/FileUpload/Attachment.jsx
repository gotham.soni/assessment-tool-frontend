import React, { Component } from 'react';
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import {Growl} from 'primereact/growl';
import {FileUpload} from 'primereact/fileupload';

class Attachment extends Component {
        
    constructor() {
        super();
        
        this.onUpload = this.onUpload.bind(this);
        // this.onBasicUpload = this.onBasicUpload.bind(this);
        // this.onBasicUploadAuto = this.onBasicUploadAuto.bind(this);
    }

    onUpload(event) {
        this.growl.show({severity: 'info', summary: 'Success', detail: 'File Uploaded'});
    }

    render() {
        return (
            <div>
                {/* <div className="content-section introduction">
                    <div className="feature-intro">
                        <h1>FileUpload</h1>
                        <p>FileUpload is an advanced uploader with dragdrop support, multi file uploads, auto uploading, progress tracking and validations.</p>
                    </div>
                </div> */}

                <div className="content-section implementation">
                    <h3>Attach File</h3>
                    <FileUpload name="demo[]" url="http://localhost:4000/uploads/upload" onUpload={this.onUpload} 
                                multiple={true} accept="image/*" maxFileSize={1000000} />
                                
                    <Growl ref={(el) => { this.growl = el; }}></Growl>
                </div>
            </div>
        )
    }
}

export default Attachment;

