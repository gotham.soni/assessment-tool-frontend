import React from 'react';
import { Accordion, AccordionTab } from 'primereact/accordion';


export function getEstimates(estimates) {
    try {
        return (
            <div>
                <p><strong><span style={{ color: "black" }}>Estimates</span></strong></p>
                {estimates.map((estimate, index) =>
                    <div key={index}>
                        <ul>
                            <li><span style={{ color: "black" }}>Integration Systems Team Assessment at {estimate.granularity}, done by Rajesh Karunamurthy</span></li>
                            <li><span style={{ color: "black" }}>Effort -&nbsp; {estimate.effort} </span></li>
                            <li><span style={{ color: "black" }}>Estimate - {estimate.cost} ($4K per week)</span></li>
                        </ul>
                    </div>
                )}
            </div>
        )

    } catch (error) {
    }
};

export function getModules(estimates) {
    try {
        return (

            <div>
                <p><strong><span style={{ color: "black" }}>Impacts - Scope</span></strong></p>
                <ul>
                    {estimates.map((estimate, index) => (
                        <div key={index}>
                            {estimate.modules.map((emodule, index) =>
                                <div key={`${emodule}`+index}>
                                    <li><strong><span style={{ color: "black" }}>{emodule.name}</span></strong><span style={{ color: "black" }}> - {emodule.type}</span><span style={{ color: "black" }}> - {emodule.effort}</span></li>
                                    <ul>
                                        <li><span style={{ color: "black" }}>{emodule.changes}</span></li>
                                    </ul>
                                </div>
                            )}
                        </div>
                    ))}
                </ul>
            </div>

        )

    } catch (error) {

    }
}

export function getOthers(estimates) {
    try {
        return (

            <div>
                <ul>
                    {estimates.map((estimate, index) => (
                        <div key={index}>
                            {estimate.others.map((eother, i) =>
                                <div key={i}>
                                    <li><strong><span style={{ color: "black" }}>{eother.name}</span></strong><span style={{ color: "black" }}> - {eother.type}</span><span style={{ color: "black" }}> - {eother.effort}</span></li>
                                    <ul>
                                        <li><span style={{ color: "black" }}>{eother.changes}</span></li>
                                    </ul>
                                </div>
                            )}
                        </div>
                    ))}
                </ul>
            </div>
        )

    } catch (error) {

    }
}

export function getPeople(contacts) {
    try {
        return (
            <div>
                <li><strong><span style={{ color: "black" }}>People </span></strong>
                    <ul>
                        <li><strong><span style={{ color: "black" }}>PM - </span></strong><span style={{ color: "black" }}>{contacts.projectManager}</span></li>
                        <li><strong><span style={{ color: "black" }}>E2E Solution Architect</span></strong><span style={{ color: "black" }}> - {contacts.e2eSolutionsArchitect}</span></li>
                        <li><strong><span style={{ color: "black" }}>E2E BSA</span></strong><span style={{ color: "black" }}> - {contacts.e2eBsa}</span></li>
                    </ul>
                </li>
            </div>
        )

    } catch (error) {

    }
}

export function createContent(basicProjectDetails, additionalInformation, contacts, estimates) {

    try {

        const message = (
            <div>
                <p><strong><span style={{ color: "black" }}>Basic Project Details</span></strong></p>
                <ul>
                    <li><strong><span style={{ color: "black" }}>ITSS Request Number</span></strong><span style={{ color: "black" }}> - ITSS-{basicProjectDetails.itssNumber}</span></li>
                    <li><strong><span style={{ color: "black" }}>Theme/Project Name</span></strong><span style={{ color: "black" }}> - {basicProjectDetails.programName}</span></li>
                    <li><strong><span style={{ color: "black" }}>Stack</span></strong><span style={{ color: "black" }}> - {basicProjectDetails.stack}</span></li>
                    <li><strong><span style={{ color: "black" }}>Customer Type</span></strong><span style={{ color: "black" }}> - {basicProjectDetails.customerType}</span></li>
                    <li><strong><span style={{ color: "black" }}>Initiative Summary (From ISF Front Door Request)</span></strong></li>
                </ul>
                <p style={{ marginLeft: "27.0pt" }}><span style={{ color: "black" }}>{additionalInformation.summary}</span></p>
                <ul>
                    <li><strong><span style={{ color: "black" }}>Scope Items&nbsp; (From ISF Front Door Request) </span></strong></li>
                </ul>
                <p style={{ marginLeft: "27.0pt" }}><span style={{ color: "black" }}> {additionalInformation.scope} </span></p>
                <ul>
                    <li><strong><span style={{ color: "black" }}>Key Business Benefits (From ISF Front Door Request)</span></strong></li>
                </ul>
                <p style={{ marginLeft: "27.0pt" }}><span style={{ color: "black" }}>{additionalInformation.keyBenefits}</span></p>
                <ul>
                    <li><strong><span style={{ color: "black" }}>Solution Assumptions (From ISF Front Door Request)</span></strong>
                        <p style={{ marginLeft: "27.0pt" }}><span style={{ color: "black" }}> {additionalInformation.assumptions} </span></p>
                    </li>
                    {getPeople(contacts)}
                </ul>
                <p><span style={{ color: "black" }}>&nbsp;</span></p>
                {getEstimates(estimates.estimates)}


                <p style={{ marginLeft: "27.0pt" }}><span style={{ color: "black" }}>&nbsp;</span></p>
                {getModules(estimates.estimates)}
                {getOthers(estimates.estimates)}


                <p><span style={{ color: "black" }}>&nbsp;</span></p>

                <p>&nbsp;</p>
                <p>Thanks,<br />[TYPE NAME]</p>
            </div>
        )
        return (
            <div style={{ alignContent: "left" }}>
                <Accordion>
                    <AccordionTab header="Email Body">
                        {message}
                    </AccordionTab>
                </Accordion>
            </div>

        )

    } catch (error) {
        console.log(error)
    }

}

//export default {createContent};