import React, { Component } from "react";
import { connect } from "react-redux";
import { getFormValues, destroy } from "redux-form";
import { PropTypes } from "prop-types";

import { createContent } from "./Content";

class EmailContent extends Component {
  constructor(props) {
    super(props);
    this.state = { copied: false };
    this.onClick = this.onClick.bind(this);
  }

  componentDidMount() {
    const {
      basicProjectDetails,
      additionalInformation,
      contacts,
      estimates
    } = this.props;

    createContent(
      basicProjectDetails,
      additionalInformation,
      contacts,
      estimates
    );
  }

  onClick(e) {
    e.preventDefault();
  }

  render() {
    const {
      basicProjectDetails,
      additionalInformation,
      contacts,
      estimates
    } = this.props;

    return (
      <div>
        {createContent(
          basicProjectDetails,
          additionalInformation,
          contacts,
          estimates
        )}
      </div>
    );
  }
}

EmailContent.propTypes = {
  basicProjectDetails: PropTypes.object,

  additionalInformation: PropTypes.object,

  contacts: PropTypes.object
};

const mapStateToProps = state => ({
  assessmentForm: getFormValues("AssessmentForm")(state),
  basicProjectDetails: getFormValues("BasicProjectDetails")(state),
  additionalInformation: getFormValues("AdditionalInformation")(state),
  contacts: getFormValues("Contacts")(state),
  estimates: getFormValues("Estimates")(state)
});

export default connect(mapStateToProps, { destroy })(EmailContent);
