import React, { Component } from 'react';
import AppBar from './AppBar'
import {
    Switch,
    Route
} from "react-router-dom";

import Dashboard from './components/Assessment/Dashboard'
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
//import StickyBox from "react-sticky-box";
import './AppShell.css';
import DataDisplay from './components/Assessment/DataDisplay';
import ModuleDashboard from './components/Modules/ModuleDashboard';
import AssessmentForm from './components/Assessment/AssessmentForm';
import ModuleForm from './components/Modules/ModuleForm';
import Register from './components/Login/Register';
import Login from './components/Login/Login';
import 'bootstrap/dist/css/bootstrap.css';
import store from '../src/store/store';
import jwt_decode from "jwt-decode";
import setAuthToken from "../src/utils/setAuthToken";
import { setCurrentUser, logoutUser } from "../src/store/Login/login-actions";
import PrivateRoute from "./components/PrivateRoute/PrivateRoute";

import "@elastic/react-search-ui-views/lib/styles/styles.css";

// Check for token to keep user logged in
if (localStorage.jwtToken) {
    // Set auth token header auth
    //const token = localStorage.jwtToken;
    const token = localStorage.getItem('jwtToken');
    setAuthToken(token);
    // Decode token and get user info and exp
    const decoded = jwt_decode(token);
    // Set user and isAuthenticated
    store.dispatch(setCurrentUser(decoded));
    // Check for expired token
    const currentTime = Date.now() / 1000; // to get in milliseconds
    if (decoded.exp < currentTime) {
        // Logout user
        console.log('YOU SUCK ')
        store.dispatch(logoutUser());
        // Redirect to login
        window.location.href = "./login";
    }
}

class AppShell extends Component {
    render() {
        return (
            <div>
                <AppBar />
                    <div className={'main-window'} style={{paddingLeft:"50px", paddingRight:"100px"}}>
                        <Route exact path={"/"} component={(props) => <Login {...props}/>} />
                        <Route exact path={"/register"} component={(props) => <Register {...props} />} />
                        <Route exact path={"/login"} component={(props) => <Login {...props}/>} />
                        <Switch>
                            <PrivateRoute exact path={"/data-display"} component={DataDisplay} />
                            <PrivateRoute exact path={"/dashboard"} component={Dashboard} />
                            <PrivateRoute exact path={"/initial-assessment"} component={AssessmentForm} />
                            <PrivateRoute exact path={"/modules"} component={ModuleDashboard} />
                            <PrivateRoute exact path={"/module-form"} component={ModuleForm} />
                        </Switch>
                    </div>
            </div>
        )
    }
}

export default AppShell;

















// import React, { Component } from 'react';

// import AppBar from './AppBar'
// import {
//     Switch,
//     Route,
//     Link
// } from "react-router-dom";

// import Dashboard from './components/Assessment/Dashboard'
// import 'primereact/resources/themes/nova-light/theme.css';
// import 'primereact/resources/primereact.min.css';
// import 'primeicons/primeicons.css';
// import StickyBox from "react-sticky-box";
// import './AppShell.css';
// import DataDisplay from './components/Assessment/DataDisplay';
// import ModuleDashboard from './components/Modules/ModuleDashboard';
// import AssessmentForm from './components/Assessment/AssessmentForm';
// import ModuleForm from './components/Modules/ModuleForm';
// import Register from './components/Login/Register';
// import Login from './components/Login/Login';
// import 'bootstrap/dist/css/bootstrap.css';
// import store from '../src/store/store';
// import jwt_decode from "jwt-decode";
// import setAuthToken from "../src/utils/setAuthToken";
// import { setCurrentUser, logoutUser } from "../src/store/Login/login-actions";
// import PrivateRoute from "./components/PrivateRoute/PrivateRoute";

// import "@elastic/react-search-ui-views/lib/styles/styles.css";

// // Check for token to keep user logged in
// if (localStorage.jwtToken) {
//     // Set auth token header auth
//     //const token = localStorage.jwtToken;
//     const token = localStorage.getItem('jwtToken');
//     setAuthToken(token);
//     // Decode token and get user info and exp
//     const decoded = jwt_decode(token);
//     // Set user and isAuthenticated
//     store.dispatch(setCurrentUser(decoded));
//     // Check for expired token
//     const currentTime = Date.now() / 1000; // to get in milliseconds
//     if (decoded.exp < currentTime) {
//         // Logout user
//         console.log('YOU SUCK ')
//         store.dispatch(logoutUser());
//         // Redirect to login
//         window.location.href = "./login";
//     }
// }



// class AppShell extends Component {

//     render() {

//         return (
//             <div className={'app-shell'}>
//                 <AppBar />

//                 <div className={'main-page'}>
//                     <StickyBox offsetTop={10} offsetBottom={10} className={'sticky'} style={{ backgroundColor: '#66CC00', paddingLeft: '15px' }}>
//                         <Link className={'home-link'} to={"/data-display"}>
//                             <div style={{ fontSize: '15px' }}>DataDisplay</div>
//                         </Link>
//                         <Link className={'home-link'} to={"/dashboard"}>
//                             <div style={{ fontSize: '15px' }}>Project Assessment</div>
//                         </Link>
//                         <Link className={'home-link'} to={"/initial-assessment"}>
//                         </Link>
//                         <Link className={'home-link'} to={"/modules"}>
//                             <div style={{ fontSize: '15px' }}>Modules</div>
//                         </Link>
//                         <Link className={'home-link'} to={"/module-form"}>
//                             {/* <div style={{ fontSize: '15px' }}>Module Form</div> */}
//                         </Link>
//                     </StickyBox>

//                     <div className={'main-window'}>
//                         <Route exact path={"/"} component={Login} />
//                         <Route exact path={"/register"} component={Register} />
//                         <Route exact path={"/login"} component={Login} />
//                         <Switch>
//                             <PrivateRoute exact path={"/data-display"} component={DataDisplay} />
//                             <PrivateRoute exact path={"/dashboard"} component={Dashboard} />
//                             <PrivateRoute exact path={"/initial-assessment"} component={AssessmentForm} />
//                             <PrivateRoute exact path={"/modules"} component={ModuleDashboard} />
//                             <PrivateRoute exact path={"/module-form"} component={ModuleForm} />
//                             {/* <PrivateRoute exact path={"/dashboard"} component={() =><Dashboard/>} /> */}
//                         </Switch>
//                     </div>

//                 </div>
//             </div>
//         )
//     }
// }

// export default AppShell;
