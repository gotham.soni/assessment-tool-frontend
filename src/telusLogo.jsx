import React from "react";

const TelusSvg = () => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="132" height="24">
      <title>logo/telus-white copy</title>
      <g fill="none">
        <g fill="#fff">
          <g>
            <g>
              <g>
                <path
                  id="Shape"
                  d="M72.9 6.46v-2.5h-8.97v12.9h8.98v-2.6h-5.33v-2.87H72V8.87h-4.43V6.46h5.34M88.75 14.32h-5.6V3.95h-3.7v12.92h9.3v-2.55M118.38 8.87c-2.48-.56-3.35-.62-3.35-1.6 0-.92 1.23-1.2 1.94-1.2 1.15 0 2.5.26 3.87.97l1-2.38c-1.4-.77-3.22-1.1-4.96-1.1-3.43 0-5.72 1.4-5.82 4.1-.1 2.46 1.9 3.4 3.76 3.77 1.78.36 3.58.68 3.62 1.73.03.98-.85 1.4-2.5 1.4-1.53 0-2.96-.5-4.4-1.1l-.86 2.58c1.67.7 3.37 1.1 5.12 1.1 2.44 0 4.2-.38 5.52-1.6.9-.84 1.2-1.86 1.2-2.83 0-2.03-1.27-3.18-4.14-3.83M58.72 6.5V3.95h-11.3V6.5h3.82v10.37h3.66V6.5h3.82M101.3 3.95v8.2c0 .65 0 .83-.04 1.08-.12.78-.88 1.24-2.1 1.25-.9 0-1.54-.28-1.85-.74-.14-.24-.22-.68-.22-1.37V3.95h-3.8v8.9c0 1.24.15 1.8.7 2.5.93 1.15 2.72 1.76 5.2 1.76h.13c3.32 0 5.03-1.3 5.53-2.5.24-.57.26-.85.26-2.03V3.95h-3.8"
                />
                <g id="Shape">
                  <path d="M49.15.4l-21.4 4.26v1.5l.06-.03C41.96 2.7 49.15.63 49.2.63c.08-.02.1-.1.1-.14 0-.06-.07-.1-.15-.1" />
                  <path d="M31.18 1.6c.34-.2.43-.08.33.16-.14.38-.87 2.7-6.26 7.1-1.4 1.16-2.4 1.8-4.43 3C24.14 7.22 28.98 3.04 31.2 1.6zm.57-1.6c-1.05.06-2.97 1.6-4.03 2.5-4.2 3.65-7.34 7-9.62 10.5-4.74 2.53-10.8 5.13-16.8 7.17l-.14.05L0 23.32l.66-.25c3.82-1.45 10.2-4.18 15.98-7.2-.32.85-.5 1.65-.5 2.38 0 .54.1 1.04.28 1.5.37.9 1.08 1.6 2.08 2 1.48.6 3.5.6 5.8-.1 5.9-1.78 12.87-7.24 14.38-8.33l.06-.04c.06-.04.07-.1.04-.15-.03-.05-.1-.06-.16-.03l-.07.04c-1.63.98-9.56 5.68-14.7 7.05-2.63.7-4.63.3-5.4-.74-.25-.36-.38-.82-.38-1.35 0-1.03.5-2.34 1.43-3.8.87-.5 1.68-.98 2.4-1.44 5.03-2.82 10.82-8.3 11.07-11.64 0-.02 0-.05 0-.07 0-.3-.13-.6-.34-.8-.24-.25-.55-.37-.88-.35z" />
                </g>
                <path
                  id="Shape"
                  d="M27.67 4.68L5.17 9.5l-.88 2.34 23.44-5.7h.04v-1.5l-.1.04"
                />
              </g>
              <path d="M128.14 2.93c1.64 0 3.02 1.3 3.02 2.98 0 1.74-1.37 3.03-3.02 3.03-1.64 0-3.03-1.3-3.03-3.02.02-1.68 1.4-2.97 3.04-2.97zm0 5.55c1.4 0 2.47-1.1 2.47-2.57 0-1.43-1.06-2.53-2.46-2.53s-2.48 1.1-2.48 2.54c0 1.48 1.08 2.58 2.48 2.58zm-1.17-4.3h1.37c.83 0 1.23.32 1.23 1 0 .62-.4.9-.93.95l1 1.54h-.58l-.96-1.5h-.57v1.5h-.55v-3.5zm.55 1.55h.57c.47 0 .9-.03.9-.58 0-.47-.4-.54-.76-.54h-.72v1.13z" />
            </g>
          </g>
        </g>
      </g>
    </svg>
  );
};

export default TelusSvg;
