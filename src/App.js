import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter } from "react-router-dom";
import './AppShell.css';
import 'bootstrap/dist/css/bootstrap.css';
import store from '../src/store/store';
import jwt_decode from "jwt-decode";
import setAuthToken from "../src/utils/setAuthToken";
import { setCurrentUser, logoutUser } from "../src/store/Login/login-actions";


import AppShell from './AppShell';

if (localStorage.jwtToken) {
    // Set auth token header auth
    //const token = localStorage.jwtToken;
    const token = localStorage.getItem('jwtToken');
    setAuthToken(token);
    
    // Decode token and get user info and exp
    const decoded = jwt_decode(token);
    // Set user and isAuthenticated
    store.dispatch(setCurrentUser(decoded));
    
    // Check for expired token
    const currentTime = Date.now() / 1000; // to get in milliseconds
    if (decoded.exp < currentTime) {
        // Logout user
        console.log('YOU SUCK ')
        store.dispatch(logoutUser());
        // Redirect to login
        window.location.href = "./login";
    }
}



const App = () => {
    return (
        <Provider store={store}>
            <BrowserRouter>
                    <AppShell />
            </BrowserRouter>
        </Provider>
    )
};

export default App;
