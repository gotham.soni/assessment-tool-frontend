
export const unwrapData = (axiosRequest) => {
    return new Promise((resolve, reject)=>{
        axiosRequest
            .then(res=> resolve(res.data))
            .catch(error => reject(error));
    })
};