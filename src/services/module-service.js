import { axiosInstance } from "../config";
import { unwrapData } from "./service-utils";

export const fetchModules = (sort) => {
    if(!sort){ sort = {sortBy: false, order: "asc"} }
    const { sortBy, order } = sort;
    return unwrapData(
        axiosInstance.get(`/modules/getModules/${sortBy}/${order}`)
    )
}

export const postNewModule = (newModule) => {
    return unwrapData(
        axiosInstance.post(`/modules/putModule`, { newModule })
    )
}

export const updateModuleForm = (module, moduleId) => {
    return unwrapData(
        axiosInstance.post(`/modules/updateModule/${moduleId}`, { module })
    )
}

