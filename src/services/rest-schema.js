import { dateToLocalArray } from "fullcalendar/src/datelib/marker";
import { number } from "../../../../AppData/Local/Microsoft/TypeScript/3.5/node_modules/@types/prop-types";

const MODELS = {
    initial_assessment_form:{
        uuid: String,                        //uuid
        theme_number: Number || null, 
        itss_number: Number,
        rpp_number: Number, 
        theme_name: String  || null,
        project_name: String  || null,
        stack: String,
        project_description: String || null,
        project_manager: String || null,
        e2e_solutions_architect: String || null,
        e2e_bsa: String || null,
        project_estimator: String || null,              // person
        project_effort: Number,                 // effort in weeks
        project_estimate: Number,               // dollar amount
        initiative_summary: [String],
        scope_items: [String],
        key_benefits: [String],
        solution_assumptions: [String],
        module_impact:[
            {
                task: String,
                module: String,
                effort: Number,               //effort estimate in weeks
                Details: String
            }
        ],
        assumptions: String  || null,
        capture_date: String,
        project_status: String,
        asked_release: String,
        actual_release: String,
    },

    tshirt_sizing_form:{
        project_initiation_date: String,
        ia_date: String,
        ia_team: String,
        release_date: String,
        current_gate: String,                   // Synonymous to status
        time: Number,
        accuracy: Number,
        ds_contact: String,
        scope_items:[
            {
                scope_item:[
                    {
                        task:String,
                        module:String,
                        total_effort: Number,
                        details:[
                            {
                                task:String,
                                effort:Number,
                            }
                        ]
                    }
                ]
            }
        ],
        assumption:[String],
        d_and_s_impact:{
            build_effort: Number,               //in days
            build_breakdown:[                         // all numbers down below are days
                {
                    team: String,
                    analysis:Number,
                    design: Number,
                    build_and_ut: Number,
                    at: Number,
                    staging: Number,
                    deploy: Number
                }
            ],
            test_effort: Number,
            test_breakdown:[                         // all numbers down below are days
                {
                    team: String,
                    analysis:Number,
                    design: Number,
                    build_and_ut: Number,
                    at: Number,
                    staging: Number,
                    deploy: Number
                }
            ],
        },
        project_schedule:{
            bsa: Number,
            technology_architect: Number,
            systems_analyst: Number,
            developer_analyst: Number,
            team_manager: Number,
            deployment_prime: Number,
            total:Number,
        },
        supporting_bt_team_estiamte:{                       //in man days
            ea_da_sa: Number,
            dba: Number,
            pm: Number,
            em: Number,
            is_ops: Number,
            qa: Number,
            cost_estimate: Number
        },
        other_costs:{
            vendor_cost: Number,
            hardware_cost: Number,
            license_cost: Number,
            other_cost: Number,
            total: Number
        }
    }
}