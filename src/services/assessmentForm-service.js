import { axiosInstance } from "../config";
import { unwrapData } from "./service-utils";

export const fetchAssessmentForms = sort => {
    if(!sort){ sort = {sortBy: false, order: "asc"} }
    const { sortBy, order } = sort;
    return unwrapData(
        axiosInstance.get(`/forms/getForm/${sortBy}/${order}`)
    )
}

export const postAssessmentForm = (form) => {
    return unwrapData(
        axiosInstance.post(`/forms/putForm`, { form })
    )
}

export const updateAssessmentForm = (form, formId) => {
    return unwrapData(
        axiosInstance.post(`/forms/updateForm/${formId}`, {
            name: form.name,
            themeNumber: form.themeNumber,
            itssNumber: form.itssNumber,
            rppNumber: form.rppNumber,
            programName: form.programName,
            stack: form.stack,
            customerType: form.customerType,
            status: form.status,
            startDate: form.startDate,
            releaseDate: form.releaseDate,

            //"_comment2": "Additional Details",
            summary: form.summary,
            scope: form.scope,
            benefits: form.benefits,
            assumptions: form.assumptions,

            //"_comment3": "Contacts",
            projectManager: form.projectManager,
            e2eSolutionsArchitect: form.e2eSolutionsArchitect,
            e2eBsa: form.e2eBsa,
            estimates: form.estimates,
            resourceProfile: form.resourceProfile,
            author: form.author

        })
    )
}

