import { axiosInstance } from "../config";
import { unwrapData } from "./service-utils";

export const getSearchResults = (query, component, sort) => {
  return unwrapData(
    axiosInstance.get(
      `/search/search_${component}/${query}/${sort.sortBy}/${sort.order}`
    )
  );
};
