import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { logoutUser } from "./store/Login/login-actions";
import TelusSvg from "./telusLogo";

class AppBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false
    };
  }

  onLogoutClick = e => {
    e.preventDefault();
    this.props.logoutUser();
    return <Redirect to="/" />;
  };

  render() {
    return (
      <div>
        <Navbar
          collapseOnSelect
          expand="lg"
          style={{ background: "#4B286D", color: "white" }}
          className="navbar"
        >
          <TelusSvg /> &nbsp;&nbsp;&nbsp;
          <Navbar.Brand href="/" style={{ color: "white" }}>
            Integrations Task Tool
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Link href="/data-display" style={{ color: "white" }}>
                Home
              </Nav.Link>
              <Nav.Link href="/dashboard" style={{ color: "white" }}>
                Assessments
              </Nav.Link>
              <Nav.Link href="/modules" style={{ color: "white" }}>
                Modules
              </Nav.Link>
            </Nav>
            <Nav>
              <Nav.Link href="/login" style={{ color: "white" }}>
                {" "}
                Login{" "}
              </Nav.Link>
              <Nav.Link
                eventKey={2}
                onClick={this.onLogoutClick}
                href="/"
                style={{ color: "white" }}
              >
                {" "}
                Logout{" "}
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </div>
    );
  }
}

AppBar.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};
const mapStateToProps = state => ({
  auth: state.auth
});
export default connect(mapStateToProps, { logoutUser })(AppBar);
